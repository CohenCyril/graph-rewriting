COQTHEORIES := $(shell find . -not -path "./deprecated/*" -not -path "./_opam/*" -not -path "./category-theory/*" -not -path "./hierarchy-builder/*" -iname '*.v')

all: Makefile.coq
	make -f Makefile.coq all

Makefile.coq: _CoqProject
	(echo "-R ./Theory Rewriting"; \
	echo $(COQTHEORIES)) > _CoqProject
	$(COQBIN)coq_makefile -f _CoqProject -o Makefile.coq

clean: Makefile.coq
	$(MAKE) -f Makefile.coq clean

fullclean: clean
	rm -f Makefile.coq Makefile.coq.conf .Makefile.coq.d