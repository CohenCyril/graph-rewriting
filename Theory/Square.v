(*
Copyright 2022 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Square *)
(** Category of spans commuting with a given cospan *)

Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.
Require Import Span.
Require Import Cospan.
Require Export Subcategory.

Local Open Scope cat.

Section Square_cat.

Context {𝐂: cat} [A B: 𝐂] (c: Cospan A B).

Definition Square_obj (s: Span A B) :=
  sp_l c ∘ sp_l s = sp_r c ∘ sp_r s.

Program Definition Square_subcat: Subcat := {|
  sobj := Square_obj;
  shom := fun _ _ _ _ _ => True
|}.

Definition Square := Sub Square_subcat.

Definition Build_square (s: Span A B)
  (comm: sp_l c ∘ sp_l s = sp_r c ∘ sp_r s): Square.
  unshelve econstructor.
  simpl.
  apply s.
  simpl.
  apply comm.
Defined.

End Square_cat.

Lemma Square_mapP {𝐂: cat} (A B: 𝐂) (c: Cospan A B) (s1 s2 : Square c) (f g : s1 ~> s2) :
  sp_map (proj1_sig f) = sp_map (proj1_sig g) <-> f = g.
Proof.
  split.
  intro.
  apply Sub_mapP.
  by apply Span_mapP.
  intro.
  by rewrite H.
Qed.