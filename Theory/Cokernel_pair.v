From mathcomp Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Morphism.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Terminal.
Require Import Unique.
Require Import VanKampen.
Require Import Reg_mono.
Require Import Isomorphism.
Require Import Subobject.
Require Import Equalizer.

Local Open Scope cat.

Definition Get_cokernel_pair {𝐂: Cat_po_along_reg_mono.type} [A B: 𝐂]
  [m: A ~> B] (rm_m: Reg_mono m): ∃ C (i j: B ~> C), Pushout m m i j
  := (po_along_reg_mono m m rm_m).

Lemma Cokernel_codiagonal {𝐂: cat} [A B C: 𝐂] [a: A ~> B]
  [b c: B ~> C]: Pushout a a b c ->
  ∃! d: C ~> B, idmap = d ∘ b ∧ idmap = d ∘ c.
Proof.
  intro po.
  apply (ump_po po idmap idmap eq_refl).
Qed.

Lemma Cokernel_codiagonal_epi {𝐂: cat} [A B C: 𝐂] [a: A ~> B]
  [b c: B ~> C] (d: C ~> B): Pushout a a b c ->
  idmap = d ∘ b -> idmap = d ∘ c -> Epi d.
Proof.
  intros po eq1 eq2.
  intros z g1 g2 eq.
  apply (f_equal (fun x => x ∘ b)) in eq.
  by rewrite !compoA -eq1 !comp1o in eq.
Qed.

Lemma Cokernel_rm_coproj {𝐂: cat}: forall [A B C: 𝐂] [a: A ~> B]
  [b c: B ~> C], Pushout a a b c -> Reg_mono b ∧ Reg_mono c.
Proof.
  assert (H: forall (A B C: 𝐂) (a: A ~> B)
    (b c: B ~> C), Pushout a a b c -> Reg_mono b).
    intros A B C a b c po.
    pose proof (d := Cokernel_codiagonal po).
    exists C.
    exists idmap.
    exists (b ∘ d).
    constructor.
    by rewrite compo1 compoA -('d) comp1o.
    intros.
    exists (d ∘ e').
    by rewrite compo1 compoA in H.
    intros.
    by rewrite H0 -compoA -('d) compo1.
  intros A B C a b c po.
  split.
  apply (H _ _ _ _ _ _ po).
  apply (H _ _ _ _ _ _ (Po_sym po)).
Qed.

Lemma Codiagonal_fact {𝐂: cat} [A B C: 𝐂] [a: A ~> B] [i1 i2: B ~> C] [e: C ~> B]
  (cokern: Pushout a a i1 i2) (eq_e1: idmap = e ∘ i1)
  (eq_e2: idmap = e ∘ i2): forall [D] (d: C ~> D), d ∘ i1 = d ∘ i2 ->
  ∃ f: B ~> D, d = f ∘ e.
Proof.
  intros.
  unshelve epose (d' := ump_po cokern (d ∘ i1) (d ∘ i2) _).
  by rewrite H.
  exists (d ∘ i1).
  apply (Unicity d').
  auto.
  split.
  by rewrite compoA -eq_e1 comp1o.
  by rewrite compoA -eq_e2 comp1o.
Qed.

Lemma Cokernel_equalizer {𝐂: cat_pb} [A B C: 𝐂] [a: A ~> B] [i1 i2: B ~> C] (cokern: Pushout a a i1 i2):
  ∃ D (d: D ~> B), Equalizer i1 i2 d.
Proof.
  pose (e := ump_po cokern idmap idmap eq_refl).
  destruct (Get_pb i1 i2) as [D [d [d' pb]]].
  simpl in D.
  assert (d' = d).
  assert (e ∘ i2 ∘ d' = e ∘ i1 ∘ d).
  by rewrite !compoA (pb_comm pb).
  by rewrite -!('e) !compo1 in H.
  subst.
  exists D.
  exists d.
  apply (Pb_equalizer pb).
Qed.

Lemma Epi_cokern {𝐂: cat} [A B: 𝐂] (e: A ~> B): Epi e ↔ Pushout e e idmap idmap.
Proof.
  split.
  { intros epi_e.
  apply Build_Pushout; auto.
  intros Y jA jB eq.
  destruct (epi_e _ jA jB eq).
  exists jA.
  by rewrite comp1o.
  intros v [eq_v _].
  by rewrite comp1o in eq_v. }

  intros po.
  intros z g1 g2 eq.
  destruct (ump_po po g1 g2 eq) as [u [eq_u1 eq_u2] _].
  rewrite comp1o in eq_u1, eq_u2.
  by rewrite eq_u2.
Qed.

Lemma Eq_cokern_invert {𝐂: cat} [A B C D: 𝐂] [e: A ~> B] [i1 i2: B ~> C] [d: D ~> B]
  (equalizer_d: Equalizer i1 i2 d) (cokern: Pushout e e i1 i2):
  Pushout e e idmap idmap ↔ ∃ d', d ∘ d' = idmap.
Proof.
  split.
  { intros po.
  destruct (unique_pushout cokern po) as [iso [eq_iso1 eq_iso2] _].
  apply iso_eq_switch_l in eq_iso1, eq_iso2.
  rewrite comp1o in eq_iso1, eq_iso2.
  subst.
  destruct equalizer_d.
  pose (d' := Eq_prop B idmap eq_refl).
  exists d'.
  by rewrite -('d'). }
  intros [d' eq_d'].
  apply Epi_cokern.
  intros z g1 g2 eq.
  destruct (ump_po cokern g1 g2 eq) as [u [eq_u1 eq_u2] unique_u].
  rewrite eq_u1 eq_u2.
  by rewrite -(comp1o i1) -(comp1o i2) -eq_d' -(compoA d') Eq_comp !compoA.
Qed.

Lemma Cokern_fact {𝐂: cat} [A B C D: 𝐂] [a: A ~> B] [b: B ~> C] [c: C ~> D]
  [d: C ~> D]: Pushout (b ∘ a) (b ∘ a) c d -> c ∘ b = d ∘ b
  -> Pushout b b c d.
Proof.
  intros po comm.
  apply Build_Pushout.
  assumption.
  intros D' b' d' comm'.
  apply (ump_po po b' d').
  by rewrite -compoA comm' compoA.
Qed.