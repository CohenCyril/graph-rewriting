Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Morphism.
Require Import Span.
Require Import Pushout.
Require Import Equalizer.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Unique.
Require Import Isomorphism.
Require Import Reg_mono.

Local Open Scope cat.

Section Split_mono.

Context {𝐂: cat} [A B: 𝐂].

Definition Split_mono (m: A ~> B) :=
  ∃ r: B ~> A, r ∘ m = idmap.

Lemma Split_mono_reg (m: A ~> B): Split_mono m -> Reg_mono m.
Proof.
  intros [r eq_rm].
  exists B.
  exists (m ∘ r).
  exists idmap.
  constructor.
  by rewrite compo1 compoA eq_rm comp1o.
  intros E' e' eq_e'.
  rewrite compo1 in eq_e'.
  exists (r ∘ e').
  by rewrite -compoA.
  intros v eq_v.
  by rewrite -eq_e' eq_v -!compoA eq_rm compo1 eq_rm compo1.
Qed.

Lemma Iso_epi_split (i: A ~> B): (∃ i': B ~> A, Iso_eq i i')
  ↔ Epi i ∧ Split_mono i.
Proof.
  split.
  intros [i' isiso].
  split.
  apply (Iso_epi (Build_Iso i i' isiso)).
  exists i'.
  by rewrite isiso.
  intros [epi_i [r split_i]].
  exists r.
  constructor.
  apply epi_i.
  cbn.
  by rewrite compoA split_i comp1o compo1.
  assumption.
Qed.

End Split_mono.
