Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Unique.

Local Open Scope cat.

Definition Product {𝐂: precat} (A B P: 𝐂)
    (pA: P ~> A) (pB: P ~> B) :=
    forall Q (qA: Q ~> A) (qB: Q ~> B),
    ∃! u: Q ~> P, pA ∘ u = qA ∧ pB ∘ u = qB.

Definition Coproduct {𝐂: precat} (A B C: 𝐂)
    (iA: A ~> C) (iB: B ~> C) :=
    forall D (jA: A ~> D) (jB: B ~> D),
    ∃! u: C ~> D, u ∘ iA = jA ∧ u ∘ iB = jB.