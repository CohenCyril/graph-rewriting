(*
Copyright 2022 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Equalizers *)

Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Pullback.
Require Import Unique.
Require Import Isomorphism.

Local Open Scope cat.

Section Equalizer.

  Context {𝐂: precat}.
  Context [A B E: 𝐂].

  Class Equalizer (f g: A ~> B) (e: E ~> A) := {
      Eq_comp: f ∘ e = g ∘ e;

      Eq_prop: forall (E': 𝐂) (e': E' ~> A), f ∘ e' = g ∘ e' ->
       ∃! ε: E' ~> E,
        e' = e ∘ ε
    }.

End Equalizer.

Arguments Eq_comp {_} [_ _ _ _ _ _] _.
Arguments Eq_prop {_} [_ _ _ _ _ _] _ [_] _ _.

Lemma Pb_equalizer {𝐂: cat} [A B C: 𝐂] [e: A ~> B] [f g: B ~> C] (pb: Pullback f g e e):
  Equalizer f g e.
Proof.
  constructor.
  by apply pb_comm.
  intros.
  pose (ɛ := ump_pb pb e' e' H).
  exists (ɛ :> E' ~> A).
  apply ('ɛ).
  intros.
  by apply (''ɛ).
Qed.

Lemma Equalizer_unique {𝐂: cat} [A A' B C: 𝐂]
  [e: A ~> B] [e': A' ~> B] [f g: B ~> C]:
  Equalizer f g e -> Equalizer f g e' -> A ≅ A'.
Proof.
  intros eq_e eq_e'.
  destruct (Eq_prop eq_e' e (Eq_comp eq_e)) as [u eq_u unique_u].
  exists u.
  pose proof (Eq_prop eq_e e' (Eq_comp eq_e')) as [u' eq_u' unique_u'].
  exists u'.
  split.
  rewrite eq_u in eq_u'.
  pose proof (Eq_prop eq_e' e' (Eq_comp eq_e')).
  apply (Unicity X (u ∘ u') idmap).
  by rewrite -compoA.
  by rewrite comp1o.
  rewrite eq_u' in eq_u.
  pose proof (Eq_prop eq_e e (Eq_comp eq_e)).
  apply (Unicity X (u' ∘ u) idmap).
  by rewrite -compoA.
  by rewrite comp1o.
Qed.