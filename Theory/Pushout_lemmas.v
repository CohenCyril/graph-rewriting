(*
Copyright 2022 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Lemmas on pushouts *)

Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Isomorphism.
Require Import Morphism.
Require Import Span.
Require Import Pushout.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Terminal.
Require Import Unique.
Require Import Square.

Local Open Scope cat.

Section Unicity.

  (** ** Unicity of pushouts *)

Context {𝐂: cat}.
Context [A B C X Y: 𝐂].
Context [f: C ~> A] [g: C ~> B] [iA: A ~> X]
        [iB: B ~> X] [jA: A ~> Y] [jB: B ~> Y].

Lemma unique_pushout:
  forall (po1: Pushout f g iA iB) (po2: Pushout f g jA jB),
  ∃! h: X ≅ Y, h¹ ∘ iA = jA ∧ h¹ ∘ iB = jB.
Proof.
    intros.
    pose proof (u := unique_pullback (𝐂 := 𝐂^op) po1 po2).
    exists (iso_op' u).
    split; apply ('u).
    intros v eq_v.
    by rewrite ((''u) (iso_op v) eq_v) iso_op'_op.
Qed.

End Unicity.

Section Square_composition.

  (** ** Composition and decomposition of pushouts *)
  (* Quiver diagram: https://q.uiver.app/#q=WzAsNixbMSwwLCJCIl0sWzIsMCwiQyJdLFsyLDEsIkMnIl0sWzAsMSwiQSciXSxbMSwxLCJCJyJdLFswLDAsIkEiXSxbMCwxLCJiIl0sWzEsMiwiXFxnYW1tYSJdLFszLDQsImEnIiwyXSxbNCwyLCJiJyIsMl0sWzUsMCwiYSJdLFs1LDMsIlxcYWxwaGEiLDJdLFswLDQsIlxcYmV0YSIsMV1d *)
  Context {𝐂: cat}.
  Context [A A' B B' C C': 𝐂].
  Context [a: A ~> B] [b: B ~> C]
          [α: A ~> A'] [β: B ~> B'] [γ: C ~> C']
          [a': A' ~> B'] [b': B' ~> C'].

  Definition Po_comp (po1: Pushout a α β a') (po2: Pushout b β γ b'):
      Pushout (b ∘ a) α γ (b' ∘ a')
    := Po_sym (Pb_comp (𝐂 := 𝐂^op) (Po_sym po1) (Po_sym po2)).

  Definition Po_decomp (comm2: γ ∘ b = b' ∘ β) (po1: Pushout a α β a')
      (po12: Pushout (b ∘ a) α γ (b' ∘ a')):
      Pushout b β γ b'
    := Po_sym (Pb_decomp (𝐂 := 𝐂^op)
      (eq_sym comm2) (Po_sym po1) (Po_sym po12)).

End Square_composition.

Section Epis.

(** ** Pushouts and epimorphisms *)

Context {𝐂: cat}.
Context [A B C D: 𝐂].
Context [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D].

Lemma Epi_stable_PO (po: Pushout f g iA iB): Epi g -> Epi iA.
Proof.
  apply (Mono_stable_PB (𝐂 := 𝐂^op) (Po_sym po)).
Qed.

Lemma Epi_trivial_po: Epi f ↔ Pushout f f idmap idmap.
Proof.
  apply (Mono_trivial_pb (𝐂 := 𝐂^op)).
Qed.

Lemma Pushout_epi [E: 𝐂] [e: E ~> C] (epi_e: Epi e):
  Pushout (f ∘ e) (g ∘ e) iA iB -> Pushout f g iA iB.
Proof.
  apply (Pullback_mono (𝐂 := 𝐂^op) epi_e).
Qed.

End Epis.

Section Isos.

(** ** Results for pushouts and isomorphisms *)

Context {𝐂: cat}.
Context [A B C D: 𝐂].
Context [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D].

Lemma Pushout_iso_A [A'] (iso: A' ≅ A) (f': C ~> A'): 
  Pushout f' g (iA ∘ iso¹) iB ↔ Pushout (iso¹ ∘ f') g iA iB.
Proof.
  apply (Pullback_iso_A (𝐂 := 𝐂^op) (iso_op iso)).
Qed.

Lemma Pushout_iso_C [C'] (iso: C' ≅ C) (f': C' ~>_𝐂 A):
  Pushout (f' ∘ iso⁻¹) g iA iB ↔ Pushout f' (g ∘ iso¹) iA iB.
Proof.
  apply (Pullback_iso_C (𝐂 := 𝐂^op) (iso_op iso)).
Qed.

Lemma Pushout_iso_D [D'] (iso: D' ≅ D) (iA': A ~> D'):
  Pushout f g (iso¹ ∘ iA') iB ↔ Pushout f g iA' (iso⁻¹ ∘ iB).
Proof.
  apply (Pullback_iso_D (𝐂 := 𝐂^op) (iso_op iso)).
Qed.

Lemma Pushout_iso_D' [D'] (iso: D ≅ D'):
  Pushout f g iA iB ↔ Pushout f g (iso¹ ∘ iA) (iso¹ ∘ iB).
Proof.
  apply (Pullback_iso_D' (𝐂 := 𝐂^op) (iso_op iso)).
Qed.

Lemma Pushout_along_iso (iso: C ≅ A):
  Pushout iso¹ g (g ∘ iso⁻¹) idmap.
Proof.
  apply (Pullback_along_iso (𝐂 := 𝐂^op) _ (iso_op iso)).
Qed.

Lemma Iso_stable_po (iso: C ≅ A): Pushout iso¹ g iA iB
  -> IsIso iB.
Proof.
  intro po.
  destruct (Iso_stable_pb (𝐂 := 𝐂^op) (iso_op iso) po) as [iB' isiso].
  exists iB'.
  by destruct isiso.
Qed.

End Isos.

Arguments Pushout_along_iso {_} [_ _ _] _ _.

Lemma Pullback_along_iso_is_po {𝐂: cat} [A B C D: 𝐂] [iso: A ≅ C]
  [g: B ~> C] [pA: D ~> A] [pB: D ~> B]:
  Pullback iso¹ g pA pB -> Pushout pA pB iso¹ g.
Proof.
  assert (po_iso: Pushout (iso⁻¹ ∘ g) idmap iso¹ g).
    apply Build_Pushout.
    by rewrite comp1o -compoA iso_eq compo1.
    intros E qA qB comm.
    exists (qA ∘ iso⁻¹).
    split.
    by rewrite compoA iso_eq comp1o.
    by rewrite compoA comm comp1o.
    intros v [eq_v1 _].
    by rewrite eq_v1 compoA iso_eq comp1o.
  pose (pb_iso := Pullback_along_iso g iso).
  intro pb.
  destruct (unique_pullback pb pb_iso) as [u [eq_u1 eq_u2] _].
  rewrite -eq_u1 -eq_u2 in po_iso.
  apply Pushout_iso_C in po_iso.
  by rewrite compoA (iso_eq u) comp1o in po_iso.
Qed.

Lemma Pushout_along_iso_is_pb {𝐂: cat} [A B C D: 𝐂] [iso: C ≅ A]
  [g: C ~> B] [iA: A ~> D] [iB: B ~> D]:
  Pushout iso¹ g iA iB -> Pullback iA iB iso¹ g.
Proof.
  apply (Pullback_along_iso_is_po (𝐂 := 𝐂^op) (g := g) (iso := iso_op iso)).
Qed.

Lemma Stable_po_switch {𝐂: cat} [A B C D E: 𝐂] [f: C ~> A]
  [g: C ~> B] [iA: A ~> D] [iB: B ~> D] [jA: A ~> E] [jB: B ~> E]
  (po: Pushout f g iA iB) (po': Pushout f g jA jB):
  Stable_po po -> Stable_po po'.
Proof.
  intro stb_po.
  pose proof (iso := unique_pushout po po').
  intros A' B' C' E' f' g' jA' jB' α β χ ϵ pb1 pb2 pb3 pb4 eq_top.
  rewrite -('iso) in pb3.
  rewrite -('iso) in pb4.
  apply Pullback_iso_C in pb3, pb4.
  apply (stb_po _ _ _ _ _ _ _ _ _ _ _ _ pb1 pb2 pb3 pb4 eq_top).
Qed.

Lemma Pushout_along_iso_stable {𝐂: cat} [A B C D: 𝐂] [g: C ~> B]
  [iso: C ≅ A] [iA: A ~> D] [iB: B ~> D] (po: Pushout iso¹ g iA iB):
    Stable_po po.
Proof.
  pose (po_iso := Pushout_along_iso g iso).
  assert (stb_po_iso: Stable_po po_iso).
  repeat intro.
  pose (Iso_stable_pb (Iso_id _) (Pb_sym pb4)).
  promote_iso iB'.
  pose (Iso_stable_pb iso (Pb_sym pb1)).
  promote_iso f'.
  pose (po' := Pushout_along_iso g' f').
  simpl in po'.
  apply (Pushout_iso_D' iB') in po'.
  simpl in po'.
  by rewrite comp1o -compoA -eq_top compoA
    (iso_to_from (iso_eq f')) comp1o in po'.
  by apply (Stable_po_switch po_iso).
Qed.

Lemma Po_of_Pb_is_Pb {𝐂: cat} [A B C: 𝐂] (f: C ~> A) (g: C ~> B):
  (∃ D (iA: A ~> D) (iB: B ~> D), Pullback iA iB f g) ->
  forall D' (iA': A ~> D') (iB': B ~> D'), Pushout f g iA' iB' ->
  Pullback iA' iB' f g.
Proof.
  intros Epb D' iA' iB' po.
  pose (Pb_of_Po_is_Po (𝐂 := 𝐂^op) f g).
  exact (p Epb D' iA' iB' po).
Qed.

(* Quiver diagram: https://q.uiver.app/#q=WzAsNixbMCwwLCJDIl0sWzIsMCwiQiJdLFswLDEsIkEiXSxbMiwxLCJEIl0sWzEsMCwiWCJdLFsxLDEsIlgnIl0sWzAsNCwiZ18xIl0sWzQsMSwiZ18yIl0sWzEsMywiaV9CIl0sWzAsMiwiZiIsMl0sWzIsMywiaV9BIiwyLHsiY3VydmUiOjJ9XSxbNSwzLCJpX3tBMn0iLDAseyJzdHlsZSI6eyJib2R5Ijp7Im5hbWUiOiJkYXNoZWQifX19XSxbNCw1LCJcXHhpIl0sWzIsNSwiaV97QTF9Il1d *)
Lemma po_fill {𝐂: cat} [A B C X X' D: 𝐂] [f: C ~> A]
  [g1: C ~> X] [g2: X ~> B] [iA1: A ~> X'] [iA: A ~> D]
  [iB: B ~> D] [ξ: X ~> X']:
  Pushout f (g2 ∘ g1) iA iB -> Pushout f g1 iA1 ξ ->
  ∃ (iA2: X' ~> D),
  iA = iA2 ∘ iA1 ∧ Pushout ξ g2 iA2 iB.
Proof.
  apply (pb_fill (𝐂 := 𝐂^op)).
Qed.

Lemma Stable_po_comp {𝐂 : cat_pb} [A B C D E F: 𝐂] [a : A ~> B] [b : B ~> C]
  [c : A ~> D] [d : C ~> F] [e : D ~> E] [f: E ~> F] [g : B ~> E] 
  [po1: Pushout a c g e] [po2: Pushout b g d f]
  [po12: Pushout (b ∘ a) c d (f ∘ e)]:
  Stable_po po1 -> Stable_po po2 -> Stable_po po12.
Proof.
  intros stb_po1 stb_po2.
  intros C' D' A' F' b'a' c' d' f'e' χ δ α ϕ pb_back12 pb_left pb_right pb_front12 comm_top12.
  destruct (pb_split pb_back12) as [B' [a' [b' [β [eq_b'a' [pb_back1 pb_back2]]]]]].
  destruct (pb_split pb_front12) as [E' [e' [f' [ϵ [eq_f'e' [pb_front1 pb_front2]]]]]].
  subst.
  destruct (Fill_cube_pb (po_comm po2) pb_back2 pb_right pb_front2) as [g' [pb_mid comm_top2]].
  rewrite -compoA in comm_top12.
  pose proof (comm_top1 := Cube_pasting_comm_top1 comm_top2 comm_top12 (po_comm po1)
    (pb_comm pb_back1) (pb_comm pb_mid) (pb_comm pb_left) pb_front1 pb_front12).
  pose (po_top1 := stb_po1 _ _ _ _ _ _ _ _ _ _ _ _ pb_back1 pb_left
  pb_mid pb_front1 comm_top1).
  pose (po_top2 := stb_po2 _ _ _ _ _ _ _ _ _ _ _ _ pb_back2 pb_mid
    pb_right pb_front2 comm_top2).
  apply (Po_comp po_top1 po_top2).
Qed.

(* Quiver diagram: https://q.uiver.app/#q=WzAsMTIsWzAsMywiQSJdLFsyLDMsIkIiXSxbNCwzLCJDIl0sWzEsNSwiRCJdLFszLDUsIkUiXSxbNSw1LCJGIl0sWzIsMCwiQiciXSxbNCwwLCJDJyJdLFs1LDIsIkYnIl0sWzMsMiwiRSciXSxbMCwwLCJBJyJdLFsxLDIsIkQnIl0sWzAsMSwiYSIsMix7ImxhYmVsX3Bvc2l0aW9uIjozMH1dLFsxLDIsImIiLDIseyJsYWJlbF9wb3NpdGlvbiI6MzB9XSxbMCwzLCJjIl0sWzIsNSwiZCIsMl0sWzMsNCwiZSJdLFs0LDUsImYiXSxbMSw0LCJnIiwxXSxbNywyLCJcXGNoaSIsMV0sWzYsMSwiXFxiZXRhIiwxXSxbOCw1LCJcXHZhcnBoaSIsMV0sWzksNCwiXFx2YXJlcHNpbG9uIiwxXSxbNiw3LCJiJyIsMV0sWzcsOCwiZCciLDFdLFs5LDgsImYnIiwxLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzYsOSwiZyciLDFdLFsxMCw2LCJhJyIsMV0sWzEwLDExLCJjJyIsMV0sWzExLDksImUnIiwxLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzExLDMsIlxcZGVsdGEiLDFdLFsxMCwwLCJcXGFscGhhIiwxXV0= *)
Lemma Stable_po_decomp {𝐂: cat_pb} [A B C D E F: 𝐂] [a : A ~> B] [b : B ~> C]
  [c : A ~> D] [d : C ~> F] [e : D ~> E] [f: E ~> F] [g : B ~> E] 
  [po1: Pushout a c g e] (po2: Pushout b g d f)
  [po12: Pushout (b ∘ a) c d (f ∘ e)]:
  Stable_po po1 -> Stable_po po12 -> Stable_po po2.
Proof.
  intros stb_po1 stb_po12.
  intros C' E' B' F' b' g' d' f' χ ϵ β ϕ pb_back2 pb_mid pb_right pb_front2 comm_top2.
  destruct (Get_pb ϵ e) as [D' [e' [δ pb_front1]]].
  destruct (Get_pb δ c) as [A' [c' [α pb_left]]].
  simpl in D', A'.
  destruct (Fill_cube_pb (eq_sym (po_comm po1)) pb_left pb_front1 pb_mid)
    as [a' [pb_back1 comm_top1]].
  pose proof (pb_front := Pb_comp pb_front2 pb_front1).
  pose proof (pb_back := Pb_comp pb_back2 pb_back1).
  pose proof (po_top1 := stb_po1 _ _ _ _ _ _ _ _ _ _ _ _ pb_back1
    pb_left pb_mid pb_front1 (eq_sym comm_top1)).
  unshelve epose proof (po_top := stb_po12 _ _ _ _ _ _ _ _ _ _ _ _ pb_back
    pb_left pb_right pb_front _).
  by rewrite compoA comm_top1 -2!compoA comm_top2.
  apply (Po_decomp comm_top2 po_top1 po_top).
Qed.

Lemma Stable_po_sym {𝐂: cat} [A B C D: 𝐂] [f: C ~> A] [g: C ~> B]
  [iA: A ~> D] [iB: B ~> D] (po: Pushout f g iA iB):
  Stable_po po ↔ Stable_po (Po_sym po).
Proof.
  split.
  intro stb_po.
  intros B' A' C' D' g' f' iB' iA' β α χ δ pb_g pb_f pb_iB pb_iA comm.
  apply (Po_sym (stb_po _ _ _ _ _ _ _ _ _ _ _ _
    pb_f pb_g pb_iA pb_iB (eq_sym comm))).
  intro stb_po.
  intros B' A' C' D' g' f' iB' iA' β α χ δ pb_g pb_f pb_iB pb_iA comm.
  apply (Po_sym (stb_po _ _ _ _ _ _ _ _ _ _ _ _
    pb_f pb_g pb_iA pb_iB (eq_sym comm))).
Qed.

Lemma Pushout_joint_epi {𝐂: cat} [A B C D: 𝐂] [f: C ~> A]
  [g: C ~> B] [iA: A ~> D] [iB: B ~> D]: Pushout f g iA iB ->
    joint_epi iA iB.
  Proof.
  intro po.
  intros E x y eq1 eq2.
  unshelve epose (ump_po po (x ∘ iA) (x ∘ iB) _).
  by rewrite compoA (po_comm po) -compoA.
  by apply (Unicity u).
Qed.

Lemma lemma22 {𝐂: cat_pb} [A A0 A1 A2 A' A1' A2' B B': 𝐂]
  [f: A ~> B] [f': A' ~> B'] [p: A' ~> A] [q: B' ~> B] [a1: A1 ~> A]
  [a1': A1' ~> A'] [a2: A2 ~> A] [a2': A2' ~> A'] [b1: A0 ~> A1]
  [b2: A0 ~> A2] [p1: A1' ~> A1] [p2: A2' ~> A2]
  (po: Pushout b1 b2 a1 a2) (stb_po: Stable_po po)
  (pb1: Pullback p a1 a1' p1) (pb1': Pullback q (f ∘ a1) (f' ∘ a1') p1)
  (pb2: Pullback p a2 a2' p2) (pb2': Pullback q (f ∘ a2) (f' ∘ a2') p2)
  (comm: f ∘ p = q ∘ f'):
  Pullback q f f' p.
Proof.
  destruct (Get_pb f q) as [A'' [q' [f'' pb_A'']]].
  simpl in A''.
  pose proof (p' := ump_pb pb_A'' p f' comm).
  unshelve epose proof (a1'' := ump_pb pb_A'' (a1 ∘ p1) (f' ∘ a1') _).
  by rewrite (pb_comm pb1') compoA.
  unshelve epose proof (a2'' := ump_pb pb_A'' (a2 ∘ p2) (f' ∘ a2') _).
  by rewrite (pb_comm pb2') compoA.
  destruct (Get_pb p1 b1) as [A0' [b1' [p0 pb_A0'_1]]].
  simpl in A0'.
  destruct (Fill_cube_pb (po_comm po) pb_A0'_1 pb1 pb2)
    as [b2' [pb_A0'_2 comm']].
  pose proof (pb_b1' := Pb_along_id b1').
  pose proof (pb_b2' := Pb_along_id b2').
  assert (pb_A1': Pullback q' a1 a1'' p1).
    apply (Pb_decomp (eq_sym (fst ('(a1'')))) (Pb_sym pb_A'')).
    by rewrite -('(a1'')).
  assert (pb_A2': Pullback q' a2 a2'' p2).
    apply (Pb_decomp (eq_sym (fst ('(a2'')))) (Pb_sym pb_A'')).
    by rewrite -('(a2'')).
  assert (eq_a''1: (a1'': A1' ~> A'') = p' ∘ a1').
    apply (Pullback_joint_mono pb_A'').
    by rewrite -('(a1'')) -compoA -('(p')) (pb_comm pb1).
    by rewrite -('(a1'')) -compoA -('(p')).
  assert (eq_a''2: (a2'': A2' ~> A'') = p' ∘ a2').
    apply (Pullback_joint_mono pb_A'').
    by rewrite -('(a2'')) -compoA -('(p')) (pb_comm pb2).
    by rewrite -('(a2'')) -compoA -('(p')).
  assert (comm'': a1'' ∘ b1' = a2'' ∘ b2').
    by rewrite eq_a''1 eq_a''2 2!compoA comm'.
  pose proof (po' := stb_po _ _ _ _ _ _ _ _ _ _ _ _
    pb_A0'_1 pb_A0'_2 pb1 pb2 comm').
  unshelve epose proof (po'' := stb_po _ _ _ _ _ _ _ _ _ _ _ _
    pb_A0'_1 pb_A0'_2 pb_A1' pb_A2' comm'').
  pose proof (u := unique_pushout po' po'').
  assert (eq_p'_u: (p': A' ~> A'') = (u: A' ≅ A'')¹).
  apply (Unicity (ump_po po' a1'' a2'' comm'')); auto.
  split; symmetry; apply ('(u)).
  rewrite (fst ('(p'))) -(comp1o q).
  apply Pb_sym.
  eapply Pb_comp.
  apply pb_A''.
  rewrite eq_p'_u -(comp1o (u: A' ≅ A'')¹).
  apply (Pullback_iso_A u).
  rewrite -eq_p'_u -('(p')).
  apply Pb_along_id.
Qed.
