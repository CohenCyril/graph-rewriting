Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.

Local Open Scope cat.

Section Subcat.

Context {𝐂 : cat}.

Record Subcat := {
  sobj : 𝐂 → Prop;
  shom {x y : 𝐂} : sobj x → sobj y → (x ~> y) → Prop;
  scomp {x y z : 𝐂} (ox : sobj x) (oy : sobj y) (oz : sobj z)
        {f : y ~> z} {g : x ~> y} :
    shom oy oz f → shom ox oy g → shom ox oz (f ∘ g);
  sid {x : 𝐂} (ox : sobj x) : shom ox ox (@idmap 𝐂 x)
}.

Variable (S: Subcat).

Definition Sub := {x: 𝐂 | sobj S x}.

HB.instance Definition _ := IsQuiver.Build Sub
  (fun x y => { f : proj1_sig x ~> proj1_sig y | @shom S _ _ (proj2_sig x) (proj2_sig y) f }).
HB.instance Definition _ := Quiver_IsPreCat.Build Sub (fun x => (exist _ idmap (sid S (proj2_sig x))))
  (fun x y z f g => (exist _ (proj1_sig g ∘ proj1_sig f) (scomp S (proj2_sig x) (proj2_sig y) (proj2_sig z) (proj2_sig g) (proj2_sig f)))).

Lemma Sub_mapP (s1 s2 : Sub) (f g : s1 ~> s2) :
  proj1_sig f = proj1_sig g <-> f = g.
Proof.
  split.
  { intro.
  destruct f, g.
  simpl in *.
  revert s.
  rewrite H.
  intro.
  assert (s = s0).
  apply Prop_irrelevance.
  rewrite H0.
  reflexivity. }
  intro.
  rewrite H.
  reflexivity.
Qed.

Definition Subcomp1o: forall (a b : Sub) (f : a ~> b), idmap \; f = f.
  intros.
  apply Sub_mapP.
  destruct a, b, f.
  simpl in *.
  apply comp1o.
Defined.

Definition Subcompo1: forall (a b : Sub) (f : a ~> b),  f \; idmap = f.
  intros.
  apply Sub_mapP.
  destruct a, b, f.
  simpl in *.
  apply compo1.
Defined.

Definition SubcompoA: forall (a b c d : Sub) (f : a ~> b) (g : b ~> c) (h : c ~> d),
f \; (g \; h) = (f \; g) \; h.
  intros.
  apply Sub_mapP.
  destruct a, b, c, d, f, g, h.
  simpl in *.
  apply compoA.
Defined.

HB.instance Definition _ := PreCat_IsCat.Build Sub Subcomp1o Subcompo1 SubcompoA.
End Subcat.

Section Full_subcat.

Context {𝐂 : cat}.

Definition Full_subcat (sobj: 𝐂 -> Prop):= Sub (Build_Subcat sobj
  (fun _ _ _ _ _ => True) (fun _ _ _ _ _ _ _ _ _ _ => I) (fun _ _ => I)).

End Full_subcat.
