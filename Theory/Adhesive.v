From mathcomp Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Morphism.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Terminal.
Require Import Unique.
Require Import VanKampen.
Require Import Reg_mono.
Require Import Isomorphism.
Require Import Subobject.
Require Import Equalizer.
Require Import Product.

Local Open Scope cat.

HB.mixin Record Cat_is_Rm_Q_Adhesive 𝐂 of Cat_pb 𝐂 & Cat_po_along_reg_mono 𝐂 := {
  stable_po [A B C D: 𝐂]
    [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D] (po: Pushout f g iA iB):
    Reg_mono f -> Stable_po po;
  po_is_pb [A B C D: 𝐂]
    [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D]: Pushout f g iA iB ->
    Reg_mono f -> Pullback iA iB f g 
}.
Unset Universe Checking.
#[short(type="rm_q_adhesive")]
HB.structure Definition Rm_Q_Adhesive: Set :=
  { 𝐂 of Cat_is_Rm_Q_Adhesive 𝐂 & }.
Set Universe Checking.

Arguments stable_po {_} [_ _ _ _ _ _ _ _] _ rm_f [_ _ _ _ _ _ _ _ _ _ _ _]: rename.
Arguments po_is_pb {_} [_ _ _ _ _ _ _ _] po rm_f: rename.

(*HB.mixin Record Rm_Q_Adhesive_is_Q_Adhesive 𝐂 of Rm_Q_Adhesive 𝐂 := {
  Q_Adhesive_mono_is_reg: forall [A B: 𝐂] (m: A ~> B), Mono m -> Reg_mono m
}.
Unset Universe Checking.
#[short(type="q_adhesive")]
HB.structure Definition Q_Adhesive: Set :=
  { 𝐂 of Rm_Q_Adhesive_is_Q_Adhesive 𝐂 }.
Set Universe Checking.*)

Definition Subobj_bin_union {𝐂: cat} [A B C Z: 𝐂]
  (a: A ~> Z) (b: B ~> Z) (c: C ~> Z) :=
  ∃ I (p: I ~> A) (u: A ~> C) (q: I ~> B) (v: B ~> C),
  Pullback a b p q ∧ Pushout p q u v
  ∧ c ∘ u = a ∧ c ∘ v = b.

HB.mixin Record Rm_Q_Adhesive_is_Rm_Adhesive 𝐂 of Rm_Q_Adhesive 𝐂 := {
  (*po_mono_VK: forall [A B C D: 𝐂] [f: C ~> A] [g: C ~> B] [iA: A ~> D]
  [iB: B ~> D] (rm_f: Reg_mono f) (po: Pushout f g iA iB),
    VanKampen f g iA iB po*)
  rm_subobj_stb_union: forall [A B C Z: 𝐂] [a: A ~> Z] [b: B ~> Z]
    (rm_a: Reg_mono a) (rm_b: Reg_mono b) (c: C ~> Z),
    Subobj_bin_union a b c -> Reg_mono c
}.

Unset Universe Checking.
#[short(type="rm_adhesive")]
HB.structure Definition Rm_Adhesive: Set :=
  { 𝐂 of Rm_Q_Adhesive_is_Rm_Adhesive 𝐂 & }.
Set Universe Checking.

(*Arguments po_mono_VK [_ _ _ _ _ _ _ _ _].*)

HB.mixin Record Rm_Adhesive_is_Adhesive 𝐂 of Rm_Adhesive 𝐂 := {
  mono_is_reg: forall [A B: 𝐂] (m: A ~> B), Mono m -> Reg_mono m
}.
Unset Universe Checking.
#[short(type="adhesive")]
HB.structure Definition Adhesive: Set :=
  { 𝐂 of Rm_Adhesive_is_Adhesive 𝐂 & }.
Set Universe Checking.

(*HB.factory Record Cat_is_Adhesive 𝐂 of Cat_pb 𝐂 := {
  po_mono_VK: forall [A B C D: 𝐂] [f: C ~> A] [g: C ~> B]
    [iA: A ~> D] [iB: B ~> D] (mono_f: Mono f) (po: Pushout f g iA iB),
    VanKampen f g iA iB po;
  po_along_mono: forall [A B C: 𝐂] (f: C ~> A) (g: C ~> B)
    (mono_f: Mono f), ∃ D (iA: A ~> D) (iB: B ~> D), Pushout f g iA iB
}.
HB.builders Context 𝐂 of Cat_is_Adhesive 𝐂.

HB.instance Definition _ := Cat_has_po_along_reg_mono.Build 𝐂
  (fun (A B C: 𝐂) (f: C ~> A) (g: C ~> B) (rm_f: Reg_mono f) =>
    po_along_mono A B C f g (Reg_mono_is_mono _ rm_f)).

Lemma VanKampen_stable: forall [A B C D: 𝐂] (f: C ~> A) (g: C ~> B)
  (iA: A ~> D) (iB: B ~> D)
  (po: Pushout f g iA iB), VanKampen f g iA iB po -> Stable_po po.
Proof.
  intros A B C D f g iA iB po VK.
  unfold Stable_po.
  intros.
  apply Pb_sym in pb1, pb2, pb3, pb4.
  destruct (VK _ _ _ _ f' g' iA' iB' α β χ δ); auto;
    by apply pb_comm; apply Pb_sym.
Qed.

Lemma VanKampen_pb: forall [A B C D: 𝐂] (f: C ~> A) (g: C ~> B) (iA: A ~> D) (iB: B ~> D)
  (po: Pushout f g iA iB) (mono_f: Mono f), VanKampen f g iA iB po -> Pullback iA iB f g.
  intros A B C D f g iA iB po mono_f VK.
  by destruct (VK_mono _ _ _ _ po mono_f VK).
Qed.

HB.instance Definition _ := Cat_is_Rm_Q_Adhesive.Build 𝐂
  (fun (A B C D: 𝐂)
  (f: C ~> A) (g: C ~> B) (iA: A ~> D) (iB: B ~> D) (po: Pushout f g iA iB) (rm_f: Reg_mono f) =>
  VanKampen_stable _ _ _ _ po (po_mono_VK _ _ _ _ _ _ _ _ (Reg_mono_is_mono f rm_f) po))
  (fun (A B C D: 𝐂)
  (f: C ~> A) (g: C ~> B) (iA: A ~> D) (iB: B ~> D) (po: Pushout f g iA iB) (rm_f: Reg_mono f) =>
  VanKampen_pb _ _ _ _ po (Reg_mono_is_mono f rm_f) (po_mono_VK _ _ _ _ _ _ _ _ (Reg_mono_is_mono f rm_f) po)).
HB.instance Definition _ := Rm_Q_Adhesive_is_Rm_Adhesive.Build 𝐂 (fun (A B C D: 𝐂)
(f: C ~> A) (g: C ~> B) (iA: A ~> D) (iB: B ~> D) (rm_f: Reg_mono f) (po: Pushout f g iA iB) =>
  po_mono_VK _ _ _ _ _ _ _ _ (Reg_mono_is_mono f rm_f) po).

Lemma Mono_is_reg: forall [A B: 𝐂] (m: A ~> B) (mono_m: Mono m), Reg_mono m.
  intros.
  destruct (po_along_mono _ _ _ m m mono_m) as [C [f [g po]]].
  simpl in C.
  exists C.
  exists f.
  exists g.
  pose (pb := VanKampen_pb _ _ _ _ po mono_m (po_mono_VK _ _ _ _ _ _ _ _ mono_m po)).
  destruct (Destruct_Pullback pb) as [e unicity].
  constructor; auto.
  intros.
  pose (unicity E' e' e' H).
  exists (u:> E' ~> A).
  apply ('u).
  intros.
  by apply (''u).
Qed.

HB.instance Definition _ := Rm_Adhesive_is_Adhesive.Build 𝐂
  (fun (A B: 𝐂) (m: A ~> B) (mono_m: Mono m) =>
  Mono_is_reg m mono_m).
HB.end.*)

(*Lemma Adhesive_VK {𝐂: adhesive}:
  forall (A B C D: 𝐂) (m: C ~> A) (g: C ~> B) (iA: A ~> D) (iB: B ~> D)
  (mono_m: Mono m) (po: Pushout m g iA iB),
  VanKampen m g iA iB po.
Proof.
  intros.
  apply po_mono_VK.
  by apply mono_is_reg.
Qed.
*)