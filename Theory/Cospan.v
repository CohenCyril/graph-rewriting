(*
Copyright 2022 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Cospans *)
(** Definition of cospans as spans in the opposite category *)

Set Warnings "-notation-overridden".

Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.
Require Import Span.

Local Open Scope cat.

Definition Cospan {𝐂: precat} (A B: 𝐂) := @Span 𝐂^op A B.

Definition cosp_obj {𝐂: precat} [A B: 𝐂] (cosp: Cospan A B): 𝐂
  := sp_obj cosp.
Definition cosp_l {𝐂: precat} [A B: 𝐂] (cosp: Cospan A B):
  A ~> cosp_obj cosp := sp_l cosp.
Definition cosp_r {𝐂: precat} [A B: 𝐂] (cosp: @Cospan 𝐂 A B):
  B ~> cosp_obj cosp := sp_r cosp.

Definition Build_cospan {𝐂: precat} [A B X: 𝐂] (l: A ~> X) (r: B ~> X):
  Cospan A B := @Build_span 𝐂^op _ _ _ l r.

HB.instance Definition _ (𝐂 : precat) (A B : 𝐂) :=
  Quiver.copy (Cospan A B) (@Span 𝐂^op A B)^op.
HB.instance Definition _ (𝐂 : cat) (A B : 𝐂) :=
  Cat.copy (Cospan A B) (@Span 𝐂^op A B)^op.

(** ** Definition of the category of cospans *)
(*
Section Cospan_quiver.

Context {𝐂: precat} (A B: 𝐂).

Definition Cospan_hom: Cospan A B -> Cospan A B -> Type
  := @Span_hom 𝐂^op A B.

HB.instance Definition _ := IsQuiver.Build (Cospan A B) Cospan_hom.

End Cospan_quiver.

Section Cospan_cat.

Context {𝐂: cat} (A B: 𝐂).

Definition Cospan_id (c: Cospan A B): Cospan_hom _ _ c c
  := @Span_id 𝐂^op A B c.

Definition Cospan_comp (c1 c2 c3: Cospan A B):
  (c1 ~> c2) -> (c2 ~> c3) -> c1 ~> c3
  := @Span_comp 𝐂^op A B c1 c2 c3.

HB.instance Definition _ := IsPreCat.Build (Cospan A B) Cospan_id Cospan_comp.

*)