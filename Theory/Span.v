(*
Copyright 2022 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Spans *)

Set Warnings "-notation-overridden".

Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.

(** ** Definition of spans *)

Local Open Scope cat.

Definition Span {𝐂: quiver} A B := {obj: 𝐂 & obj ~> A &
obj ~> B}.
(*Record Span {𝐂: quiver} A B := {sp_obj: 𝐂; sp_l: sp_obj ~> A;
sp_r: sp_obj ~> B}.*)

Definition sp_obj {𝐂} [A B] (sp: @Span 𝐂 A B): 𝐂 := projT1 (sigT_of_sigT2 sp).
Definition sp_l {𝐂} [A B] (sp: @Span 𝐂 A B): sp_obj sp ~> A
  := projT2 (sigT_of_sigT2 sp).
Definition sp_r {𝐂} [A B] (sp: @Span 𝐂 A B): sp_obj sp ~> B
  := projT3 sp.

Definition Build_span {𝐂: precat} [A B X: 𝐂] (l: X ~> A) (r: X ~> B):
  Span A B := existT2 _ _ X l r.

Coercion sp_obj: Span >-> Quiver.sort.

(** ** Definition of the category of spans *)

Section Span_quiver.

Context {𝐂: precat} (A B: 𝐂).

Definition Span_hom (X Y : Span A B) :=
  {sp_map: X ~> Y |
  sp_l X = sp_l Y ∘ sp_map ∧
  sp_r X = sp_r Y ∘ sp_map
}.

Definition Build_Span_hom (X Y : Span A B)
  (sp_map: X ~> Y) (comm_l: sp_l X = sp_l Y ∘ sp_map)
  (comm_r: sp_r X = sp_r Y ∘ sp_map): Span_hom X Y :=
  exist _ sp_map (comm_l, comm_r).

Definition sp_map X Y (sp_hom: Span_hom X Y): X ~> Y :=
  proj1_sig sp_hom.

Definition sp_comm_l X Y (sp_hom: Span_hom X Y):
  sp_l X = sp_l Y ∘ (sp_map X Y sp_hom) :=
  fst (proj2_sig sp_hom).

Definition sp_comm_r X Y (sp_hom: Span_hom X Y):
  sp_r X = sp_r Y ∘ (sp_map X Y sp_hom) :=
  snd (proj2_sig sp_hom).

HB.instance Definition _ := IsQuiver.Build (Span A B) Span_hom.

End Span_quiver.

Arguments Build_Span_hom {𝐂} [A B X Y] _ _ _.
Arguments sp_map {𝐂} [A B X Y] _.
Arguments sp_comm_l {𝐂} [A B X Y] _.
Arguments sp_comm_r {𝐂} [A B X Y] _.

Section Span_cat.

Context {𝐂: cat} (A B: 𝐂).

Definition Span_id (s: Span A B): Span_hom _ _ s s :=
  Build_Span_hom idmap (eq_sym (comp1o _))
  (eq_sym (comp1o _)).

Definition Span_comp (s1 s2 s3: Span A B) (f1: s1 ~> s2) (f2: s2 ~> s3): s1 ~> s3.
  apply (Build_Span_hom ((sp_map f1) \; (sp_map f2))).
  by rewrite -compoA -2!sp_comm_l.
  by rewrite -compoA -2!sp_comm_r.
Defined.

HB.instance Definition _ := IsPreCat.Build (Span A B) Span_id Span_comp.

Lemma Span_mapP (s1 s2 : Span A B) (f g : s1 ~> s2) :
  sp_map f = sp_map g <-> f = g.
Proof.
  split.
  intro.
  destruct f, g.
  simpl in *.
  revert p.
  rewrite H.
  intros.
  f_equal;
  apply Prop_irrelevance.
  intro.
  rewrite H.
  reflexivity.
Qed.

Lemma span_cat : PreCat_IsCat (Span A B).
Proof.
  constructor.
  intros.
  rewrite -Span_mapP.
  apply comp1o.
  intros.
  rewrite -Span_mapP.
  apply compo1.
  intros.
  rewrite -Span_mapP.
  apply compoA.
Qed.

HB.instance Definition _ := span_cat.

End Span_cat.
