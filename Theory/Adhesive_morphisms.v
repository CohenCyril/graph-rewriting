From mathcomp Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Morphism.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Terminal.
Require Import Unique.
Require Import VanKampen.
Require Import Reg_mono.
Require Import Isomorphism.
Require Import Subobject.
Require Import Equalizer.
Require Import Adhesive.
Require Import Split_mono.

Local Open Scope cat.

(*Record Pre_adhesive_morph {𝐂: cat} [A C: 𝐂] (f: C ~> A) := {
  pre_adh_has_po: forall B (g: C ~> B), ∃ D (iA: A ~> D)
    (iB: B ~> D), Pushout f g iA iB;
  pre_adh_stb_po_pb: forall B (g: C ~> B) D (iA: A ~> D)
    (iB: B ~> D) (po: Pushout f g iA iB),
    Stable_po po ∧ Pullback iA iB f g
  }.*)

Definition Pre_adhesive_morph {𝐂: cat} [A C: 𝐂] (f: C ~> A) :=
  forall B (g: C ~> B), ∃ D (iA: A ~> D)
    (iB: B ~> D) (po: Pushout f g iA iB),
    Stable_po po ∧ Pullback iA iB f g.

Lemma Pre_adh_stb_po {𝐂: cat} [A B C D: 𝐂] [f: C ~> A] [g: C ~> B]
  [iA: A ~> D] [iB: B ~> D] (po: Pushout f g iA iB):
  Pre_adhesive_morph f -> Stable_po po.
Proof.
  intro pre_adh_f.
  destruct (pre_adh_f _ g) as [D' [jA [jB [po' [stb_po' _]]]]].
  by apply (Stable_po_switch po').
Qed.

Lemma Pre_adh_po_pb {𝐂: cat} [A B C D: 𝐂] [f: C ~> A] [g: C ~> B]
  [iA: A ~> D] [iB: B ~> D] (po: Pushout f g iA iB):
  Pre_adhesive_morph f -> Pullback iA iB f g.
Proof.
  intro pre_adh_f.
  destruct (pre_adh_f _ g) as [D' [jA [jB [po' [_ pb]]]]].
  destruct (unique_pushout po po') as [u [eq_u1 eq_u2] _].
  rewrite -eq_u1 -eq_u2 in pb.
  apply Pullback_iso_C in pb.
  by rewrite -compoA (iso_eq u) compo1 in pb.
Qed.

Definition Adhesive_morph {𝐂: cat} [A C: 𝐂] (f: A ~> C) :=
  forall B D (g: B ~> C) (pA: D ~> A) (pB: D ~> B) (pb: Pullback f g pA pB),
    Pre_adhesive_morph pB.

Lemma Iso_pre_adhesive {𝐂: cat} [A C: 𝐂] (f: C ≅ A): Pre_adhesive_morph f¹.
  intros B g.
  pose (po := Pushout_along_iso g f).
  exists B.
  exists (g ∘ f⁻¹).
  exists idmap.
  exists po.
  split.
  apply Pushout_along_iso_stable.
  by apply Pushout_along_iso_is_pb.
Qed.

Lemma Iso_adhesive {𝐂: cat} [A C: 𝐂] (f: A ≅ C): Adhesive_morph f¹.
Proof.
  intros B D g pA pB pb.
  pose (Iso_stable_pb _ pb).
  promote_iso pB.
  apply (Iso_pre_adhesive pB).
Qed.

Lemma Adhesive_is_Pre_adhesive {𝐂: cat} [A B: 𝐂] (f: A ~> B):
  Adhesive_morph f -> Pre_adhesive_morph f.
Proof.
  intro adh_f.
  apply (adh_f _ _ idmap idmap f).
  apply Build_Pullback.
  by rewrite compo1 comp1o.
  intros C pA pB eq.
  exists pA.
  rewrite compo1 in eq.
  rewrite compo1.
  by split.
  intros v [eq_v1 eq_v2].
  by rewrite compo1 in eq_v1.
Qed.

Lemma Pre_adhesive_is_regular_mono {𝐂: cat} [A B: 𝐂] (f: A ~> B):
  Pre_adhesive_morph f -> Reg_mono f.
  intro pre_adh_f.
  destruct (pre_adh_f _ f) as [C [iB [iB' [po [stable pb]]]]].
  exists C.
  exists iB.
  exists iB'.
  constructor.
  by apply pb_comm.
  intros.
  destruct (Destruct_Pullback pb) as [pb_comm ump_pb].
  pose (ump_pb E' e' e' H).
  exists (u :> E' ~> A).
  apply ('u).
  intros.
  by apply (''u).
Qed.

Lemma Adhesive_stable_pb {𝐂: cat} [A B C D: 𝐂] [f: A ~> C] [g: B ~> C]
  [pA: D ~> A] [pB: D ~> B] (pb: Pullback f g pA pB):
  Adhesive_morph f -> Adhesive_morph pB.
Proof.
  intro adh_f.
  intros ? ? ? ? ? pb'.
  apply (adh_f _ _ _ _ _ (Pb_comp pb pb')).
Qed.

Lemma Reg_mono_is_adh {𝐂: rm_q_adhesive} [A C: 𝐂] [f: A ~> C]:
  Reg_mono f -> Adhesive_morph f.
Proof.
  intros rm_m.
  intros B D g pA pB pb E e.
  pose (rm_pB := Reg_mono_stable_PB _ _ _ _ pb rm_m).
  destruct (po_along_reg_mono pB e rm_pB) as [F [f1 [f2 po]]].
  simpl in F.
  exists F.
  exists f1.
  exists f2.
  exists po.
  split.
  by apply stable_po.
  by apply po_is_pb.
Qed.

Lemma Adh_is_Reg_mono {𝐂: cat} [A B: 𝐂] (f: A ~> B):
  Adhesive_morph f -> Reg_mono f.
Proof.
  intro adh_f.
  apply Adhesive_is_Pre_adhesive in adh_f.
  apply (Pre_adhesive_is_regular_mono _ adh_f).
Qed.

Lemma Pre_adhesive_comp {𝐂: cat_pb} [A B C: 𝐂] [a: B ~> C] [b: A ~> B]:
  Pre_adhesive_morph a -> Pre_adhesive_morph b -> Pre_adhesive_morph (a ∘ b).
Proof.
  intros preadh_a preadh_b.
  unfold Pre_adhesive_morph in *.
  intros D c.
  destruct (preadh_b D c) as [E [d [e [po_E [stb_po_E po_E_is_pb]]]]].
  destruct (preadh_a E d) as [F [f [g [po_F [stb_po_F po_F_is_pb]]]]].
  exists F.
  exists f.
  exists (g ∘ e).
  exists (Po_comp po_E po_F).
  split.
  apply (Stable_po_comp stb_po_E stb_po_F).
  apply (Pb_comp po_F_is_pb po_E_is_pb).
Qed.

Lemma Adhesive_comp {𝐂: cat_pb} [A B C: 𝐂] [a: B ~> C] [b: A ~> B]:
  Adhesive_morph a -> Adhesive_morph b -> Adhesive_morph (a ∘ b).
Proof.
  intros adh_a adh_b.
  intros A' C' α c' χ pb.
  apply Pb_sym in pb.
  destruct (pb_split pb) as [B' [a' [b' [β [eq_c' [pb1 pb2]]]]]].
  rewrite eq_c' in pb.
  rewrite eq_c'.
  apply Pb_sym in pb, pb1, pb2.
  pose proof (preadh_b' := adh_b _ _ _ _ _ pb1).
  pose proof (preadh_a' := adh_a _ _ _ _ _ pb2).
  apply (Pre_adhesive_comp preadh_a' preadh_b').
Qed.

Lemma Adhesive_po_is_mono {𝐂: cat_pb} [A B C D: 𝐂] [m: C ~> A] [f: C ~> B]
  [g: A ~> D] [n: B ~> D] (po: Pushout m f g n):
  Adhesive_morph m -> Mono n.
Proof.
  (* For stability, suppose that m : C → A is adhesive, and consider
  a pushout [po] which is also a pullback, since m is adhesive. *)
  intro adh_m.
  pose proof (pb := Pre_adh_po_pb po (Adhesive_is_Pre_adhesive _ adh_m)).
  (* Pull back this pushout along n to get a cube in which the top face,
  like the bottom, is a pushout, and the four vertical faces are pullbacks;
  and in which n1 and n2 are the kernel pair of n. *)
  destruct (Get_pb n n) as [B' [n1 [n2 pb_right]]].
  simpl in B'.
  pose proof (pb_left :=
    fst (Mono_trivial_pb (f := m)) (Reg_mono_is_mono _
      (Adh_is_Reg_mono _ adh_m))).
  destruct (Fill_cube_pb (po_comm po) pb_left (Pb_sym pb) pb_right)
    as [f' [pb_back comm_top]].
  pose proof (stb_po := Pre_adh_stb_po po
    (Adhesive_is_Pre_adhesive _ adh_m)).
  pose proof (po_top := stb_po _ _ _ _ _ _ _ _ _ _ _ _ pb_left pb_back
    (Pb_sym pb) pb_right comm_top).
  (* Since n1 is a pushout of an isomorphism it is an isomorphism. *)
  pose (Iso_stable_po (Iso_id _) po_top).
  promote_iso n1.
  (* This implies that the kernel pair of n is trivial,
  and so that n is a monomorphism; thus any pushout of an
  adhesive morphism is a monomorphism. *)
  rewrite -(compo1 n1¹) in pb_right.
  apply (Pullback_iso_D n1) in pb_right.
  simpl in pb_right.
  pose proof (eq_id := Kernel_pair_id (n2 ∘ n1⁻¹) pb_right).
  rewrite eq_id in pb_right.
  by apply (Mono_trivial_pb (f := n)).
Qed.

Lemma Adhesive_po_is_pre_adh {𝐂: cat_pb} [A B C D: 𝐂] [m: C ~> A] [f: C ~> B]
  [g: A ~> D] [n: B ~> D] (po: Pushout m f g n):
  Adhesive_morph m -> Pre_adhesive_morph n.
Proof.
  intros adh_m E h.
  pose proof (pb := Pre_adh_po_pb po (Adhesive_is_Pre_adhesive _ adh_m)).
  destruct ((Adhesive_is_Pre_adhesive _ adh_m) E (h ∘ f))
    as [F [l [p [po_F [stb_po_F pb_C]]]]].
  destruct (po_fill po_F po) as [k [eq_l po'_F]].
  subst.
  exists F.
  exists k.
  exists p.
  exists po'_F.
  pose proof (stb_po := Pre_adh_stb_po po
    (Adhesive_is_Pre_adhesive _ adh_m)).
  split.
  apply Stable_po_sym in stb_po_F, stb_po.
  apply Stable_po_sym.
  apply (Stable_po_decomp _ stb_po stb_po_F).

  pose proof (mono_n := Adhesive_po_is_mono po adh_m).
  pose proof (pb_n := (fst (Mono_trivial_pb (f := n))) mono_n).
  unshelve eapply (Pb_sym (lemma22 _ stb_po (Pb_sym pb) (Pb_sym pb_C) pb_n
    _ (po_comm po'_F))).
  pose proof (mono_p := Adhesive_po_is_mono po_F adh_m).
  rewrite (po_comm po'_F) comp1o.
  apply Build_Pullback.
  by rewrite comp1o.
  intros Y qA qB comm.
  exists qB.
  rewrite compoA in comm.
  apply mono_p in comm.
  split.
  assumption.
  by rewrite compo1.
  intros v [eq_v1 eq_v2].
  by rewrite eq_v2 compo1.
Qed.

(* On the axioms for adhesive and quasiadhesive categories - Garner & Lack - 2012
2.3 Proposition. The class of adhesive morphisms is stable under pushout.*)
Lemma Adhesive_stable_po {𝐂: cat_pb} [A B C D: 𝐂] [m: C ~> A] [f: C ~> B]
  [g: A ~> D] [n: B ~> D] (po: Pushout m f g n):
  Adhesive_morph m -> Adhesive_morph n.
Proof.
  intros adh_m D' B' d b n' pb_B'.
  destruct (Get_pb d g) as [A' [g' [a pb_A']]].
  destruct (Get_pb a m) as [C' [m' [c pb_C']]].
  simpl in A', C'.
  destruct (Fill_cube_pb (po_comm po) pb_C' pb_A' (Pb_sym pb_B'))
    as [f' [pb'_C' comm']].
  pose proof (adh_m' := Adhesive_stable_pb (Pb_sym pb_C') adh_m).
  pose proof (stb_po := Pre_adh_stb_po po
    (Adhesive_is_Pre_adhesive _ adh_m)).
  pose proof (po_D' := stb_po _ _ _ _ _ _ _ _ _ _ _ _ pb_C' pb'_C' pb_A'
    (Pb_sym pb_B') comm').
  apply (Adhesive_po_is_pre_adh po_D' adh_m').
Qed.

Lemma Bin_union_closed_VK {𝐂: cat_pb}:
  (forall (A B C Z: 𝐂) (a: A ~> Z) (b: B ~> Z) (adh_a: Adhesive_morph a)
  (adh_b: Adhesive_morph b) (c: C ~> Z), Subobj_bin_union a b c
  -> Adhesive_morph c) ->
  (forall (A B: 𝐂) (m: A ~> B), Split_mono m -> Adhesive_morph m) ->
  forall (A B C D: 𝐂) (m: C ~> A) (f: C ~> B) (g: A ~> D) (n: B ~> D)
  (po: Pushout m f g n), Adhesive_morph m -> VanKampen _ _ _ _ po.
Proof.
  intros adh_stable_bin_union split_adh ? ? ? ? ? ? ? ? ? adh_m.
  intros ? ? ? ? m' f' g' n' a b c d eq_top eq_front eq_right
    pb_left pb_back.
  pose proof (stb_po := Pre_adh_stb_po po
    (Adhesive_is_Pre_adhesive _ adh_m)).
  split.
  { intros [pb_front pb_right].
  apply Pb_sym in pb_left, pb_back, pb_front, pb_right.
  eapply stb_po; auto.
  apply pb_left.
  apply pb_back.
  apply pb_front.
  apply pb_right. }
  intros po_top.
  pose proof (adh_m' := Adhesive_stable_pb pb_left adh_m).
  pose proof (adh_n' := Adhesive_stable_po po_top adh_m').
  pose proof (adh_n := Adhesive_stable_po po adh_m).
  pose proof (stb_po_top := Pre_adh_stb_po po_top
    (Adhesive_is_Pre_adhesive _ adh_m')).
  pose proof (pb_top := Pre_adh_po_pb po_top
    (Adhesive_is_Pre_adhesive _ adh_m')).
  pose proof (pb_bot := Pre_adh_po_pb po
    (Adhesive_is_Pre_adhesive _ adh_m)).
  split.
  2: {
  pose proof (mono_n := Reg_mono_is_mono _ (Adh_is_Reg_mono _ adh_n)).
  pose proof (mono_n' := Reg_mono_is_mono _ (Adh_is_Reg_mono _ adh_n')).
  apply Mono_trivial_pb in mono_n.
  rename mono_n into pb_n.
  apply Mono_trivial_pb in mono_n'.
  rename mono_n' into pb_n'.
  unshelve eapply (lemma22 _ stb_po_top (Pb_sym pb_top) _ pb_n' _ eq_right).
  rewrite eq_front -(pb_comm pb_back).
  apply (Pb_comp (Pb_sym pb_bot) pb_left).
  rewrite eq_right comp1o -{2}(compo1 b).
  apply (Pb_comp pb_n (Pb_sym (Pb_along_id b))).
  }
  destruct (Get_pb g' g') as [A2' [g1' [g2' pb_g']]].
  destruct (Get_pb g g) as [A2 [g1 [g2 pb_g]]].
  simpl in A2, A2'.
  unshelve eapply (lemma22 _ stb_po_top pb_g' _ pb_top _ eq_front).
  unshelve epose proof (a2 := ump_pb pb_g (a ∘ g1') (a ∘ g2') _).
  by rewrite -2!compoA -eq_front 2!compoA (pb_comm pb_g').
  rewrite ('a2) eq_front.
  apply (Pb_comp pb_g).
  admit.
  rewrite eq_right -(pb_comm pb_left).
  apply (Pb_comp pb_bot pb_back).
Admitted.
  