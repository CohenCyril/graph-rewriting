(*
Copyright 2022 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Lemmas on pullbacks *)

Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Isomorphism.
Require Import Morphism.
Require Import Span.
Require Import Pullback.
Require Import Pushout.
Require Import Terminal.
Require Import Unique.
Require Import Square.

Local Open Scope cat.


Section Unicity.

  (** ** Unicity of pullbacks *)
  Context {𝐂: cat}.
  Context [A B C X Y: 𝐂].
  Context [f: A ~> C] [g: B ~> C] [pA: X ~> A]
          [pB: X ~> B] [qA: Y ~> A] [qB: Y ~> B].

  Lemma unique_pullback:
    forall (pb1: Pullback f g pA pB) (pb2: Pullback f g qA qB),
    ∃! h: Y ≅ X, pA ∘ h¹ = qA ∧ pB ∘ h¹ = qB.
  Proof.
    intros.

    destruct pb1 as [comm1 term1].
    destruct pb2 as [comm2 term2].
    destruct (unique_terminal _ _ term1 term2) as [iso _ iso_unique].
    unshelve eexists.
    exists (sp_map (proj1_sig iso⁻¹)).
    exists (sp_map (proj1_sig iso¹)).
    destruct (iso_eq iso) as [eq_iso1 eq_iso2].
    apply Square_mapP in eq_iso1, eq_iso2.
    simpl in eq_iso1, eq_iso2.
    by constructor.

    pose (iso_eq := proj2_sig (proj1_sig iso⁻¹)).
    simpl in *.
    split; rewrite -iso_eq.

    intros.
    unshelve epose (iso_unique _ I).
    unshelve econstructor; simpl.
    unshelve econstructor; simpl.
    unshelve econstructor; simpl.
Admitted.

    (*pose proof (iso1 := ump_pb pb1 qA qB (pb_comm pb2)).
    pose proof (iso2 := ump_pb pb2 pA pB (pb_comm pb1)).
    unshelve eexists.
    unshelve econstructor.
    apply iso1.
    unshelve econstructor.
    apply iso2.
    constructor.

    apply (Unicity (ump_pb pb1 pA pB (pb_comm pb1))).
    simpl. split.
    by rewrite -compoA -('iso1) -('iso2).
    by rewrite -compoA -('iso1) -('iso2).
    by rewrite !comp1o.

    apply (Unicity (ump_pb pb2 qA qB (pb_comm pb2))).
    simpl. split.
    by rewrite -compoA -('iso2) -('iso1).
    by rewrite -compoA -('iso2) -('iso1).
    by rewrite !comp1o.

    simpl. split.
    apply ('iso1).

    intros iso' [eq_iso'1 eq_iso'2].
    by apply (''iso1).
  Qed.*)

End Unicity.

Section Square_composition.

  (** ** Composition and decomposition of pullbacks *)

  (* Quiver diagram: https://q.uiver.app/#q=WzAsNixbMSwwLCJCIl0sWzIsMCwiQyJdLFsyLDEsIkMnIl0sWzAsMSwiQSciXSxbMSwxLCJCJyJdLFswLDAsIkEiXSxbMCwxLCJiIl0sWzEsMiwiXFxnYW1tYSJdLFszLDQsImEnIiwyXSxbNCwyLCJiJyIsMl0sWzUsMCwiYSJdLFs1LDMsIlxcYWxwaGEiLDJdLFswLDQsIlxcYmV0YSIsMV1d *)
  Context {𝐂: cat}.
  Context [A A' B B' C C': 𝐂].
  Context [a: A ~> B] [b: B ~> C]
          [α: A ~> A'] [β: B ~> B'] [γ: C ~> C']
          [a': A' ~> B'] [b': B' ~> C'].

Lemma Pb_comp (pb2: Pullback γ b' b β):
  Pullback β a' a α -> Pullback γ (b' ∘ a') (b ∘ a) α.
  intros pb1.
  apply Build_Pullback.
  by rewrite -compoA (pb_comm pb2) compoA (pb_comm pb1) -compoA.
  intros A'' a'' c'' comm''.
  unshelve epose proof (u2 := ump_pb pb2 a'' (a' ∘ c'') _).
  by rewrite -compoA.
  unshelve epose proof (u1 := ump_pb pb1 u2 c'' _).
  by rewrite -('u2).
  exists (u1: A'' ~> A).
  split.
  by rewrite compoA -('u1) -('u2).
  apply ('u1).
  intros v [v_comm1 v_comm2].
  apply (''u1).
  split; auto.
  apply (''u2).
  split.
  by rewrite -compoA.
  by rewrite -compoA (pb_comm pb1) compoA v_comm2.
Qed.

Lemma Pb_decomp (comm1: β ∘ a = a' ∘ α)
  (pb2: Pullback γ b' b β) (pb12: Pullback γ (b' ∘ a') (b ∘ a) α):
  Pullback β a' a α.
Proof.
  apply Build_Pullback.
  assumption.
  intros A'' a'' α'' comm''.
  unshelve epose proof (u2 := ump_pb pb2 (b ∘ a'') (a' ∘ α'') _).
  by rewrite -comm'' -2!compoA (pb_comm pb2).
  unshelve epose proof (u12 := ump_pb pb12 (b ∘ u2) α'' _).
  by rewrite -compoA (pb_comm pb2) compoA -('u2) compoA.
  exists u12.
  split.
  apply (Unicity u2).
  by split.
  split.
  by rewrite -compoA -('u12) -('u2).
  by rewrite -compoA comm1 compoA -('u12).
  apply ('u12).
  intros u12' [u12'_comm1 u12'_comm2].
  apply (''u12).
  split; auto.
  by rewrite compoA -u12'_comm1 -('u2).
Qed.

End Square_composition.

Section Monos.

  (** ** Pullbacks and monomorphisms *)

  Context {𝐂: cat}.
  Context [A B C D: 𝐂].
  Context [f: A ~> C] [g: B ~> C] [pA: D ~> A] [pB: D ~> B].

Lemma Mono_stable_PB (pb: Pullback f g pA pB): Mono f -> Mono pB.
Proof.
  intro mono_f.
  intros E m n eq_pB.
  unshelve epose proof (u := ump_pb pb (pA ∘ m) (pB ∘ n) _).
  by rewrite -compoA (pb_comm pb) compoA eq_pB.
  apply (Unicity u); split; auto.
  apply mono_f.
  by rewrite -2!compoA (pb_comm pb) 2!compoA eq_pB.
Qed.

Lemma Mono_trivial_pb: Mono f ↔ Pullback f f idmap idmap.
Proof.
  split.
  intro mono_f.
  apply Build_Pullback.
  reflexivity.
  intros E qA qB comm.
  apply mono_f in comm.
  rewrite comm.
  exists qB.
  by rewrite compo1.
  intros v [eq_v _].
  by rewrite eq_v compo1.
  intros pb'.
  intros E h i eq_f.
  destruct (ump_pb pb' h i eq_f) as [u [eq_u1 eq_u2] _].
  by rewrite eq_u1 eq_u2.
Qed.

Lemma Pullback_mono [E: 𝐂] [m: C ~> E] (mono_m: Mono m):
  Pullback (m ∘ f) (m ∘ g) pA pB -> Pullback f g pA pB.
Proof.
  intro pb.
  apply Build_Pullback.
  apply mono_m.
  rewrite -!compoA.
  by apply pb_comm.
  intros Y qA qB eq.
  apply (ump_pb pb qA qB). 
  by rewrite !compoA eq.
Qed.  

End Monos.

Section Isos_aux.

  (** ** Auxiliary lemmas for pullbacks and isomorphisms *)

Context {𝐂: cat}.
Context [A B C D: 𝐂].
Context [f: A ~> C] [g: B ~> C] [pA: D ~> A] [pB: D ~> B].

Lemma Pullback_iso_A_aux [A'] (iso: A ≅ A') (f': A' ~>_𝐂 C): 
Pullback f' g (iso¹ ∘ pA) pB -> Pullback (f' ∘ iso¹) g pA pB.
Proof.
  intro pb.
  apply Build_Pullback.
  by rewrite compoA (pb_comm pb).
  intros E qA qB comm.
  rewrite compoA in comm.
  pose proof (u := ump_pb pb (iso¹ ∘ qA) qB comm).
  exists u.
  split.
  pose proof (eq_qA := fst ('u)).
  rewrite compoA in eq_qA.
  by apply (Iso_mono iso) in eq_qA.
  apply ('u).
  intros v [eq_v1 eq_v2].
  apply (''u).
  by rewrite eq_v1 eq_v2 compoA.
Qed.

Lemma Pullback_iso_C_aux [C'] (iso: C ≅ C') (f': A ~> C'):
Pullback (iso⁻¹ ∘ f') g pA pB -> Pullback f' (iso¹ ∘ g) pA pB.
Proof.
  intro pb.
  apply Build_Pullback.
  rewrite compoA.
  symmetry.
  apply iso_eq_switch_l.
  by rewrite -compoA (pb_comm pb).
  intros E qA qB comm.
  unshelve epose proof (u := ump_pb pb qA qB _).
  rewrite compoA.
  symmetry.
  apply iso_eq_switch_l.
  by rewrite -compoA comm.
  exists u.
  split; apply ('u).
  intros v [eq_v1 eq_v2].
  by apply (''u).
Qed.

Lemma Pullback_iso_D_aux [D'] (iso: D ≅ D') (pA': D' ~>_𝐂 A):
  Pullback f g (pA' ∘ iso¹) pB -> Pullback f g pA' (pB ∘ iso⁻¹).
Proof.
  intro pb.
  apply Build_Pullback.
  rewrite -compoA.
  apply iso_eq_switch_r.
  by rewrite compoA (pb_comm pb).
  intros E qA qB comm.
  pose proof (u := ump_pb pb qA qB comm).
  exists (iso¹ ∘ u).
  split.
  rewrite -compoA.
  apply ('u).
  rewrite compoA -(compoA u) iso_eq compo1.
  apply ('u).
  intros v [eq_v1 eq_v2].
  apply iso_eq_switch_l.
  apply (''u).
  split.
  by rewrite compoA -(compoA v) iso_eq compo1.
  by rewrite -compoA.
Qed.

Lemma Pullback_iso_D'_aux [D'] (iso: D' ≅ D): Pullback f g pA pB
  -> Pullback f g (pA ∘ iso¹) (pB ∘ iso¹).
Proof.
  intro pb.
  apply Build_Pullback.
  by rewrite -2!compoA (pb_comm pb).
  intros E qA qB comm.
  pose proof (u := ump_pb pb qA qB comm).
  exists (iso⁻¹ ∘ u).
  rewrite 2!compoA -(compoA u) iso_eq compo1.
  apply ('u).
  intros v [eq_v1 eq_v2].
  rewrite compoA in eq_v1.
  rewrite compoA in eq_v2.
  by rewrite ((''u) _ (eq_v1, eq_v2)) -compoA iso_eq compo1.
Qed.

End Isos_aux.

Section Isos.

(** ** Results for pullbacks and isomorphisms *)

Context {𝐂: cat}.
Context [A B C D: 𝐂].
Context [f: A ~> C] [g: B ~> C] [pA: D ~> A] [pB: D ~> B].

Lemma Pullback_iso_A [A'] (iso: A ≅ A') (f': A' ~>_𝐂 C): 
  Pullback f' g (iso¹ ∘ pA) pB ↔ Pullback (f' ∘ iso¹) g pA pB.
Proof.
  split.
  apply Pullback_iso_A_aux.
  intro pb.
  rewrite -(compo1 pA) -(iso_eq iso) compoA in pb.
  apply (Pullback_iso_A_aux (iso_sym iso)) in pb.
  by rewrite compoA iso_eq comp1o in pb.
Qed.

Lemma Pullback_iso_C [C'] (iso: C ≅ C') (f': A ~> C'):
  Pullback (iso⁻¹ ∘ f') g pA pB ↔ Pullback f' (iso¹ ∘ g) pA pB.
Proof.
  split.
  apply Pullback_iso_C_aux.
  intro pb.
  apply Pb_sym in pb.
  eapply (Pullback_iso_C_aux (iso_sym iso)) in pb.
  by apply Pb_sym.
Qed.

Lemma Pullback_iso_D [D'] (iso: D ≅ D') (pA': D' ~>_𝐂 A):
  Pullback f g (pA' ∘ iso¹) pB ↔ Pullback f g pA' (pB ∘ iso⁻¹).
Proof.
  split.
  apply Pullback_iso_D_aux.
  intro pb.
  apply Pb_sym in pb.
  apply (Pullback_iso_D_aux (iso_sym iso)) in pb.
  by apply Pb_sym.
Qed.

Lemma Pullback_iso_D' [D'] (iso: D' ≅ D): Pullback f g pA pB
  ↔ Pullback f g (pA ∘ iso¹) (pB ∘ iso¹).
Proof.
  split.
  apply Pullback_iso_D'_aux.
  intro pb.
  apply (Pullback_iso_D'_aux (iso_sym iso)) in pb.
  by rewrite 2!compoA iso_eq 2!comp1o in pb.
Qed.

Lemma Pullback_along_iso (iso: A ≅ C):
Pullback iso¹ g (iso⁻¹ ∘ g) idmap.
Proof.
  apply Build_Pullback.
  by rewrite comp1o -compoA iso_eq compo1.
  intros E qA qB comm.
  exists qB.
  rewrite compoA -comm -compoA iso_eq 2!compo1.
  by split.
  intros v [_ eq_v2].
  by rewrite compo1 in eq_v2.
Qed.

Lemma Iso_stable_pb (iso: A ≅ C): Pullback iso¹ g pA pB -> IsIso pB.
Proof.
  intro pb.
  pose proof (pb' := Pullback_along_iso iso).
  pose proof (u := unique_pullback pb' pb).
  exists ((u: D ≅ B)⁻¹).
  destruct u as [u [? eq_u] ?].
  simpl.
  split;
  by rewrite -eq_u compo1 iso_eq.
Qed.

End Isos.

Arguments Pullback_along_iso {_} [_ _ _] _ _.

Lemma Kernel_pair_id {𝐂: cat} [A B: 𝐂] [f: A ~> B] (i: A ~> A):
  Pullback f f idmap i -> i = idmap.
Proof.
  intro pb.
  destruct (ump_pb pb i idmap (eq_sym (pb_comm pb))) as [u [eq_i_u i_inv_i] _].
  rewrite compo1 in eq_i_u.
  symmetry in eq_i_u.
  subst.
  destruct (ump_pb pb i i eq_refl) as [u [eq_i_u i_idemp] _].
  rewrite compo1 in eq_i_u.
  subst.
  by rewrite -i_idemp in i_inv_i.
Qed.

Lemma Pb_of_Po_is_Po {𝐂: cat} [A B C: 𝐂] (iA: A ~> C) (iB: B ~> C):
  (∃ D (f: D ~> A) (g: D ~> B), Pushout f g iA iB) ->
  forall D' (pA: D' ~> A) (pB: D' ~> B), Pullback iA iB pA pB ->
  Pushout pA pB iA iB.
Proof.
  intros [D [f [g po]]] D' pA pB pb.
  apply Build_Pushout.
  apply (pb_comm pb).
  intros D'' qA qB comm.
  apply (ump_po po).
  destruct (ump_pb pb f g (po_comm po)) as [u [eq_u1 eq_u2] _].
  by rewrite eq_u1 eq_u2 -2!compoA comm.
Qed.

(* Quiver diagram: https://q.uiver.app/#q=WzAsNixbMiwwLCJBIl0sWzIsMSwiQyJdLFswLDEsIkIiXSxbMCwwLCJEIl0sWzEsMSwiWCJdLFsxLDAsIlgnIl0sWzAsMSwiZiJdLFszLDAsInBBIiwwLHsiY3VydmUiOi0yfV0sWzMsMiwicEIiLDJdLFsyLDQsImcxIiwyXSxbNCwxLCJnMiIsMl0sWzMsNSwicEExIiwyLHsic3R5bGUiOnsiYm9keSI6eyJuYW1lIjoiZGFzaGVkIn19fV0sWzUsMCwicEEyIiwyXSxbNSw0LCJcXHhpIiwyXV0= *)
Lemma pb_fill {𝐂: cat} [A B C X X' D: 𝐂] [f: A ~> C] [g1: B ~> X]
  [g2: X ~> C] [pA: D ~> A] [pA2: X' ~> A] [pB: D ~> B] [ξ: X' ~> X]:
  Pullback f (g2 ∘ g1) pA pB -> Pullback f g2 pA2 ξ ->
  ∃ (pA1: D ~> X'),
  pA = pA2 ∘ pA1 ∧ Pullback ξ g1 pA1 pB.
Proof.
  intros pb12 pb2.
  unshelve epose proof (pA1 := ump_pb pb2 pA (g1 ∘ pB) _).
  by rewrite (pb_comm pb12) compoA.
  exists pA1.
  split.
  apply ('pA1).
  rewrite ('pA1) in pb12.
  apply (Pb_decomp (eq_sym (snd ('pA1))) pb2 pb12).
Qed.

(* Quiver diagram: https://q.uiver.app/#q=WzAsNixbMiwwLCJBIl0sWzIsMSwiQyJdLFswLDEsIkIiXSxbMCwwLCJEIl0sWzEsMSwiWCJdLFsxLDAsIlgnIl0sWzAsMSwiZiJdLFszLDAsInBBIiwwLHsiY3VydmUiOi0yfV0sWzMsMiwicEIiLDJdLFsyLDQsImcxIiwyXSxbNCwxLCJnMiIsMl0sWzMsNSwicEExIiwyLHsic3R5bGUiOnsiYm9keSI6eyJuYW1lIjoiZGFzaGVkIn19fV0sWzUsMCwicEEyIiwyLHsic3R5bGUiOnsiYm9keSI6eyJuYW1lIjoiZGFzaGVkIn19fV0sWzUsNCwiXFx4aSIsMix7InN0eWxlIjp7ImJvZHkiOnsibmFtZSI6ImRhc2hlZCJ9fX1dXQ== *)
Lemma pb_split {𝐂: cat_pb} [A B C X D: 𝐂] [f: A ~> C] [g1: B ~> X] [g2: X ~> C]
  [pA: D ~> A] [pB: D ~> B]: Pullback f (g2 ∘ g1) pA pB ->
  ∃ X' (pA1: D ~> X') (pA2: X' ~> A) (ξ: X' ~> X),
  pA = pA2 ∘ pA1 ∧ Pullback ξ g1 pA1 pB ∧ Pullback f g2 pA2 ξ.
Proof.
  intro pb12.
  destruct (Get_pb f g2) as [X' [pA2 [ξ pb2]]].
  simpl in X'.
  exists X'.
  destruct (pb_fill pb12 pb2) as [pA1 [eq_pA pb1]].
  exists pA1.
  exists pA2.
  exists ξ.
  split.
  assumption.
  split.
  rewrite eq_pA in pb12.
  apply (Pb_decomp (pb_comm pb1) pb2 pb12).
  assumption.
Qed.

(* Quiver diagram: https://q.uiver.app/#q=WzAsOCxbMCwwLCJBJyJdLFsyLDAsIkInIl0sWzEsMSwiQyciXSxbMywxLCJEJyJdLFsxLDMsIkMiXSxbMywzLCJEIl0sWzAsMiwiQSJdLFsyLDIsIkIiXSxbMCwxLCJhJyJdLFsyLDMsImQnIiwyLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzEsMywiYyciXSxbMiw0LCJcXGdhbW1hIiwwLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzMsNSwiXFxkZWx0YSJdLFs0LDUsImQiLDJdLFswLDIsImInIiwyLHsic3R5bGUiOnsiYm9keSI6eyJuYW1lIjoiZGFzaGVkIn19fV0sWzAsNiwiXFxhbHBoYSIsMV0sWzYsNCwiYiIsMl0sWzEsNywiXFxiZXRhIiwyLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzYsNywiYSIsMix7ImxhYmVsX3Bvc2l0aW9uIjo3MH1dLFs3LDUsImMiLDJdLFsyLDUsIiIsMSx7InN0eWxlIjp7Im5hbWUiOiJjb3JuZXIifX1dXQ== *)
Lemma Fill_cube {𝐂: cat} [A A' B B' C C' D D': 𝐂] [a: A ~> B] [b: A ~> C]
  [c: B ~> D] [d: C ~> D] [α: A' ~> A] [β: B' ~> B] [γ: C' ~> C]
  [δ: D' ~> D] [a': A' ~> B'] [c': B' ~> D'] [d': C' ~> D']
  (comm_bot: c ∘ a = d ∘ b) (comm_back: β ∘ a' = a ∘ α) (comm_right: δ ∘ c' = c ∘ β)
  (pb_front: Pullback δ d d' γ):
  ∃ (b': A' ~> C'), γ ∘ b' = b ∘ α ∧ c' ∘ a' = d' ∘ b'.
  Proof.
  destruct (ump_pb pb_front (c' ∘ a') (b ∘ α)) as [b' [comm_top comm_left] _].
  by rewrite -compoA comm_right compoA comm_back -compoA comm_bot compoA.
  symmetry in comm_left.
  by exists b'.
Qed.

(* Quiver diagram: https://q.uiver.app/#q=WzAsOCxbMCwwLCJBJyJdLFsyLDAsIkInIl0sWzEsMSwiQyciXSxbMywxLCJEJyJdLFsxLDMsIkMiXSxbMywzLCJEIl0sWzAsMiwiQSJdLFsyLDIsIkIiXSxbMCwxLCJhJyJdLFsyLDMsImQnIiwyLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzEsMywiYyciXSxbMiw0LCJcXGdhbW1hIiwwLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzMsNSwiXFxkZWx0YSJdLFs0LDUsImQiLDJdLFswLDIsImInIiwyLHsic3R5bGUiOnsiYm9keSI6eyJuYW1lIjoiZGFzaGVkIn19fV0sWzAsNiwiXFxhbHBoYSIsMV0sWzYsNCwiYiIsMl0sWzEsNywiXFxiZXRhIiwyLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzYsNywiYSIsMix7ImxhYmVsX3Bvc2l0aW9uIjo3MH1dLFs3LDUsImMiLDJdLFsyLDUsIiIsMSx7InN0eWxlIjp7Im5hbWUiOiJjb3JuZXIifX1dLFswLDcsIiIsMSx7InN0eWxlIjp7Im5hbWUiOiJjb3JuZXIifX1dLFsxLDUsIiIsMSx7InN0eWxlIjp7Im5hbWUiOiJjb3JuZXIifX1dLFswLDQsIiIsMSx7InN0eWxlIjp7Im5hbWUiOiJjb3JuZXIifX1dXQ== *)
Lemma Fill_cube_pb {𝐂: cat} [A A' B B' C C' D D': 𝐂] [a: A ~> B] [b: A ~> C]
  [c: B ~> D] [d: C ~> D] [α: A' ~> A] [β: B' ~> B] [γ: C' ~> C]
  [δ: D' ~> D] [a': A' ~> B'] [c': B' ~> D'] [d': C' ~> D']
  (comm_bot: c ∘ a = d ∘ b) (pb_back: Pullback β a a' α) (pb_right: Pullback δ c c' β)
  (pb_front: Pullback δ d d' γ):
  ∃ (b': A' ~> C'), Pullback γ b b' α ∧ c' ∘ a' = d' ∘ b'.
  destruct (Fill_cube comm_bot (pb_comm pb_back) (pb_comm pb_right) pb_front)
    as [b' [comm_left comm_top]].
  exists b'.
  split.
  unshelve eapply (Pb_decomp comm_left pb_front _).
  rewrite -comm_bot -comm_top.
  apply (Pb_comp pb_right pb_back).
  assumption.
Qed.

(* Quiver diagram: https://q.uiver.app/#q=WzAsOCxbMCwwLCJBJyJdLFsyLDAsIkInIl0sWzEsMSwiQyciXSxbMywxLCJEJyJdLFsxLDMsIkMiXSxbMywzLCJEIl0sWzAsMiwiQSJdLFsyLDIsIkIiXSxbMCwxLCJhJyIsMCx7InN0eWxlIjp7ImJvZHkiOnsibmFtZSI6ImRhc2hlZCJ9fX1dLFsyLDMsImQnIiwyLHsibGFiZWxfcG9zaXRpb24iOjMwLCJzdHlsZSI6eyJib2R5Ijp7Im5hbWUiOiJkYXNoZWQifX19XSxbMSwzLCJjJyIsMCx7InN0eWxlIjp7ImJvZHkiOnsibmFtZSI6ImRhc2hlZCJ9fX1dLFsyLDQsIlxcZ2FtbWEiLDAseyJsYWJlbF9wb3NpdGlvbiI6MzAsInN0eWxlIjp7ImJvZHkiOnsibmFtZSI6ImRhc2hlZCJ9fX1dLFszLDUsIlxcZGVsdGEiXSxbNCw1LCJkIiwyXSxbMCwyLCJiJyIsMix7InN0eWxlIjp7ImJvZHkiOnsibmFtZSI6ImRhc2hlZCJ9fX1dLFswLDYsIlxcYWxwaGEiLDEseyJzdHlsZSI6eyJib2R5Ijp7Im5hbWUiOiJkYXNoZWQifX19XSxbNiw0LCJiIiwyXSxbMSw3LCJcXGJldGEiLDIseyJsYWJlbF9wb3NpdGlvbiI6MzAsInN0eWxlIjp7ImJvZHkiOnsibmFtZSI6ImRhc2hlZCJ9fX1dLFs2LDcsImEiLDIseyJsYWJlbF9wb3NpdGlvbiI6NzB9XSxbNyw1LCJjIiwyXSxbMiw1LCIiLDEseyJzdHlsZSI6eyJuYW1lIjoiY29ybmVyIn19XSxbMCw3LCIiLDEseyJzdHlsZSI6eyJuYW1lIjoiY29ybmVyIn19XSxbMSw1LCIiLDEseyJzdHlsZSI6eyJuYW1lIjoiY29ybmVyIn19XSxbMCw0LCIiLDEseyJzdHlsZSI6eyJuYW1lIjoiY29ybmVyIn19XV0= *)
Lemma Get_cube {𝐂: cat_pb} [A B C D D': 𝐂] (a: A ~> B) (b: A ~> C)
  (c: B ~> D) (d: C ~> D) (δ: D' ~> D) (comm: c ∘ a = d ∘ b):
  ∃ (A' B' C': 𝐂) (α: A' ~> A) (β: B' ~> B) (γ: C' ~> C)
    (a': A' ~> B') (b': A' ~> C') (c': B' ~> D') (d': C' ~> D'),
    Pullback β a a' α ∧ Pullback δ c c' β ∧ Pullback γ b b' α ∧
    Pullback δ d d' γ ∧ c' ∘ a' = d' ∘ b'.
  Proof.
  destruct (Get_pb δ c) as [B' [c' [β pb_right]]].
  destruct (Get_pb β a) as [A' [a' [α pb_back]]].
  destruct (Get_pb δ d) as [C' [d' [γ pb_front]]].
  simpl in *.
  destruct (Fill_cube_pb comm pb_back pb_right pb_front) as [b' [pb_left comm_top]].
  exists A'.
  exists B'.
  exists C'.
  exists α.
  exists β.
  exists γ.
  exists a'.
  exists b'.
  exists c'.
  by exists d'.
Qed.

(* Quiver diagram: https://q.uiver.app/#q=WzAsOSxbMCwxLCJBJyJdLFsyLDEsIkInIl0sWzEsMiwiQyciXSxbMywyLCJEJyJdLFsxLDQsIkMiXSxbMyw0LCJEIl0sWzAsMywiQSJdLFsyLDMsIkIiXSxbMiwwXSxbMCwxLCJhJyJdLFsyLDMsImQnIiwyLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzEsMywiYyciXSxbMiw0LCJcXGdhbW1hIiwwLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzMsNSwiXFxkZWx0YSJdLFs0LDUsImQiLDJdLFswLDIsImInIiwyXSxbMCw2LCJcXGFscGhhIiwxLHsic3R5bGUiOnsidGFpbCI6eyJuYW1lIjoibW9ubyJ9fX1dLFs2LDQsImIiLDJdLFsxLDcsIlxcYmV0YSIsMix7ImxhYmVsX3Bvc2l0aW9uIjozMH1dLFs2LDcsImEiLDIseyJsYWJlbF9wb3NpdGlvbiI6NzB9XSxbNyw1LCJjIiwyXSxbMiw1LCIiLDEseyJzdHlsZSI6eyJuYW1lIjoiY29ybmVyIn19XSxbMSw1LCIiLDEseyJzdHlsZSI6eyJuYW1lIjoiY29ybmVyIn19XSxbMCw0LCIiLDEseyJzdHlsZSI6eyJuYW1lIjoiY29ybmVyIn19XSxbMCw3LCIiLDEseyJzdHlsZSI6eyJuYW1lIjoiY29ybmVyIn19XV0= *)
Lemma Pb_cube_top_comm {𝐂: cat} [A A' B B' C C' D D': 𝐂] [a: A ~> B] [b: A ~> C]
  [c: B ~> D] [d: C ~> D] [α: A' ~> A] [β: B' ~> B] [γ: C' ~> C] [δ: D' ~> D]
  [a': A' ~> B'] [b': A' ~> C'] [c': B' ~> D'] [d': C' ~> D'] (Mono_α: Mono α)
  (comm: c ∘ a = d ∘ b) (pb_back: Pullback β a a' α) (pb_right: Pullback δ c c' β)
  (pb_left: Pullback γ b b' α) (pb_front: Pullback δ d d' γ): c' ∘ a' = d' ∘ b'.
Proof.
  destruct (Fill_cube_pb comm pb_back pb_right pb_front) as [b'' [pb_left' comm_top']].
  pose proof (iso := unique_pullback pb_left pb_left').
  assert (iso_id: (iso: A' ≅ A')¹ = idmap).
  apply Mono_α.
  rewrite comp1o.
  apply ('iso).
  by rewrite comm_top' -('iso) iso_id comp1o.
Qed.

Section Cubes.

(*
Quiver diagram:
https://q.uiver.app/#q=WzAsMTIsWzEsMiwiQSJdLFswLDMsIkMiXSxbMywyLCJCIl0sWzIsMywiRCJdLFs0LDMsIkYiXSxbNSwyLCJFIl0sWzUsMCwiRSciXSxbMywwLCJCJyJdLFsyLDEsIkQnIl0sWzQsMSwiRiciXSxbMCwxLCJDJyJdLFsxLDAsIkEnIl0sWzAsMSwiYyIsMV0sWzAsMiwiYSIsMSx7ImxhYmVsX3Bvc2l0aW9uIjoyMH1dLFsxLDMsImQiLDFdLFszLDQsImciLDFdLFsyLDUsImUiLDEseyJsYWJlbF9wb3NpdGlvbiI6MjB9XSxbNSw0LCJmIiwxXSxbNiw1LCJcXHZhcmVwc2lsb24iLDEseyJsYWJlbF9wb3NpdGlvbiI6MzB9XSxbNywyLCJcXGJldGEiLDEseyJsYWJlbF9wb3NpdGlvbiI6MzB9XSxbOCwzLCJcXGRlbHRhIiwxLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzksNCwiXFxwaGkiLDEseyJsYWJlbF9wb3NpdGlvbiI6MzB9XSxbMTAsMSwiXFxnYW1tYSIsMSx7ImxhYmVsX3Bvc2l0aW9uIjozMH1dLFsxMSwwLCJcXGFscGhhIiwxLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzExLDEwLCJjJyIsMV0sWzExLDcsImEnIiwxXSxbNyw2LCJlJyIsMV0sWzYsOSwiZiciLDFdLFs3LDgsImInIiwxXSxbMTAsOCwiZCciLDEseyJsYWJlbF9wb3NpdGlvbiI6ODB9XSxbOCw5LCJnJyIsMSx7ImxhYmVsX3Bvc2l0aW9uIjo4MH1dLFsyLDMsImIiLDFdLFsxMSwzLCIoMSkiLDEseyJzdHlsZSI6eyJib2R5Ijp7Im5hbWUiOiJub25lIn0sImhlYWQiOnsibmFtZSI6Im5vbmUifX19XSxbNyw0LCIoMikiLDEseyJzdHlsZSI6eyJib2R5Ijp7Im5hbWUiOiJub25lIn0sImhlYWQiOnsibmFtZSI6Im5vbmUifX19XV0=
*)

Context {𝐂: cat_pb}.
Context [A B C D E F A' B' C' D' E' F': 𝐂].
Context [a : A ~> B] [b : B ~> D] [c : A ~> C] [d : C ~> D] [e : B ~> E] [f: E ~> F]
[g: D ~> F] [a': A' ~> B'] [b': B' ~> D'] [c': A' ~> C'] [d': C' ~> D'] [e': B' ~> E']
[f': E' ~> F'] [g': D' ~> F'] [α: A' ~> A] [β: B' ~> B] [γ: C' ~> C] [δ: D' ~> D]
[ɛ: E' ~> E] [ϕ: F' ~> F].

Let comm_bot1 := b ∘ a = d ∘ c.
Let comm_top1 := b' ∘ a' = d' ∘ c'.
Let comm_front1 := δ ∘ d' = d ∘ γ.
Let comm_back1 := β ∘ a' = a ∘ α.
Let comm_left1 := γ ∘ c' = c ∘ α.
Let pb_left1 := Pullback γ c c' α.
Let pb_front1 := Pullback δ d d' γ.
Let pb_back1 := Pullback β a a' α.

Let pb_mid := Pullback δ b b' β.
Let comm_mid := δ ∘ b' = b ∘ β.

Let comm_bot2 := f ∘ e = g ∘ b.
Let comm_top2 := f' ∘ e' = g' ∘ b'.
Let pb_front2 := Pullback ϕ g g' δ.
Let pb_back2 := Pullback ɛ e e' β.
Let pb_right2 := Pullback ϕ f f' ɛ.

Let comm_bot12 := f ∘ e ∘ a = g ∘ d ∘ c.
Let comm_top12 := f' ∘ e' ∘ a' = g' ∘ d' ∘ c'.
Let pb_front12 := Pullback ϕ (g ∘ d) (g' ∘ d') γ.
Let pb_back12 := Pullback ɛ (e ∘ a) (e' ∘ a') α.

Lemma Cube_pasting_comm_top1: comm_top2 -> comm_top12 ->
  comm_bot1 -> comm_back1 -> comm_mid ->
  comm_left1 -> pb_front1 -> pb_front12 ->
  comm_top1.
Proof.
  intros commt2 commt12 commbt1 commbk1 commm comml1 pbf1 pbf12.
  destruct (Fill_cube commbt1 commbk1 commm pbf1) as [c'' [comml1' commt1']].
  assert (c' = c'').
  unshelve epose (ump_pb pbf12 (f' ∘ e' ∘ a') (c ∘ α) _).
  by rewrite commt12 -compoA (pb_comm pbf12) compoA comml1.
  apply (Unicity u).
  split.
  assumption.
  apply (symmetry comml1).
  split.
  by rewrite (compoA c'') -commt1' -(compoA a') commt2.
  apply (symmetry comml1').
  subst.
  assumption.
Qed.

End Cubes.

Lemma Pullback_joint_mono {𝐂: cat} [A B C D: 𝐂] [f: A ~> C]
  [g: B ~> C] [pA: D ~> A] [pB: D ~> B]: Pullback f g pA pB ->
  joint_mono pA pB.
Proof.
  intro pb.
  intros E x y eq1 eq2.
  unshelve epose (ump_pb pb (pA ∘ x) (pB ∘ x) _).
  by rewrite -compoA (pb_comm pb) -compoA.
  by apply (Unicity u).
Qed.