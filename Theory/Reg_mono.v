(*
Copyright 2022 Samuel Arsac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*)

(** * Regular monomorphisms *)

Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Morphism.
Require Import Span.
Require Import Pushout.
Require Import Equalizer.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Unique.
Require Import Isomorphism.

Local Open Scope cat.

(** Definition *)
Definition Reg_mono {𝐂: precat} [A B: 𝐂] (m: A ~> B) :=
  ∃ C (f g: B ~> C), Equalizer f g m.

(** Proof that a regular monomorphism is indeed a monomorphism *)
Lemma Reg_mono_is_mono: forall {𝐂: cat} [A B: 𝐂] (m: A ~> B),
  Reg_mono m -> Mono m.
  intros 𝐂 A B m [C [f [g [eq_m equal_prop]]]] D g1 g2 eq.
  destruct (equal_prop _ (m ∘ g1)) as [ϵ eq_ϵ unique_ϵ].
  - by rewrite -2!compoA eq_m.
  - assert (ϵ = g1).
    + by apply unique_ϵ.
    + rewrite -H.
      by apply unique_ϵ.
Qed.

Lemma Reg_mono_stable_PB {𝐂: cat} [A B C D: 𝐂]
  (f: A ~> C) (g: B ~> C) (pA: D ~> A) (pB: D ~> B):
  Pullback f g pA pB -> Reg_mono f -> Reg_mono pB.
Proof.
  intros pb [E [a [b eq]]].
  exists E.
  exists (a ∘ g).
  exists (b ∘ g).
  destruct eq.
  constructor.
  by rewrite compoA -(pb_comm pb) -compoA
    Eq_comp compoA (pb_comm pb) -compoA.
  intros.
  destruct (Eq_prop E' (g ∘ e')) as [a' comm_a' unique_a'].
  by rewrite -2!compoA.
  destruct (Destruct_Pullback pb) as [pb_comm ump_pb].
  destruct (ump_pb E' a' e' (symmetry comm_a')) as [u [u_comm1 u_comm2] unique_u].
  exists u; [assumption|].
  intros.
  apply unique_u.
  split; [|assumption].
  apply unique_a'.
  by rewrite -compoA pb_comm compoA -H0.
Qed.

Lemma Reg_mono_decomp {𝐂: cat} [A B C: 𝐂] (m: A ~> B)
  (n: B ~> C): Reg_mono (n ∘ m) -> Mono n -> Reg_mono m.
Proof.
  intros [D [f [g eq]]] mono_n.
  exists D.
  exists (f ∘ n).
  exists (g ∘ n).
  destruct eq.
  rewrite -2!compoA in Eq_comp.
  constructor; [assumption|].
  intros E' e' eq_e'.
  rewrite 2!compoA in eq_e'.
  pose proof (ϵ := Eq_prop E' (n ∘ e') eq_e').
  exists ϵ.
  apply mono_n.
  rewrite -compoA.
  apply ('ϵ).
  intros v eq_v.
  apply ((''ϵ) v).
  by rewrite eq_v compoA.
Qed.

Lemma Reg_mono_iso_l {𝐂: cat} [A B C: 𝐂] [m: A ~> B]
  (rm_m: Reg_mono m) (iso: B ≅ C): Reg_mono (iso¹ ∘ m).
Proof.
  destruct rm_m as [D [f [g eq]]].
  exists D.
  exists (f ∘ iso⁻¹).
  exists (g ∘ iso⁻¹).
  constructor.
  rewrite 2!compoA -2!(compoA m) !iso_eq compo1 comp1o.
  apply (Eq_comp eq).
  intros E' e' eq_e'.
  unshelve epose proof (ϵ := Eq_prop eq (iso⁻¹ ∘ e') _).
  by rewrite -!compoA eq_e'.
  exists ϵ.
  by rewrite compoA -('ϵ) -compoA iso_eq compo1.
  intros v eq_v.
  apply (''ϵ).
  by rewrite eq_v -!compoA iso_eq compo1.
Qed.

Lemma Reg_mono_iso_r {𝐂: cat} [A B C: 𝐂] [m: B ~> C]
  (rm_m: Reg_mono m) (iso: A ≅ B): Reg_mono (m ∘ iso¹).
Proof.
  destruct rm_m as [D [f [g eq]]].
  exists D.
  exists f.
  exists g.
  constructor.
  by rewrite -!compoA (Eq_comp eq).
  intros E' e' eq_e'.
  pose proof (ϵ := Eq_prop eq e' eq_e').
  exists (iso⁻¹ ∘ ϵ).
  by rewrite compoA -(compoA ϵ) iso_eq compo1 -('ϵ).
  intros.
  unshelve erewrite ((''ϵ) (iso¹ ∘ v) _).
  simpl.
  by rewrite -compoA.
  by rewrite -compoA iso_eq compo1.
Qed.

HB.mixin Record Cat_has_po_along_reg_mono 𝐂 of Cat 𝐂 := {
  po_along_reg_mono: forall (A B C: 𝐂) (m: C ~> A) (g: C ~> B) (rm_m: Reg_mono m),
  ∃ D (iA: A ~> D) (iB: B ~> D), Pushout m g iA iB
}.
Unset Universe Checking.
HB.structure Definition Cat_po_along_reg_mono: Set :=
  { 𝐂 of Cat_has_po_along_reg_mono 𝐂 &}.
Set Universe Checking.

Arguments po_along_reg_mono {_} [_ _ _] _ _ _.