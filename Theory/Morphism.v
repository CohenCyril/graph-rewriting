
Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.

Local Open Scope cat.

Definition Mono {𝐂: precat} [x y: 𝐂] (f : x ~> y) :=
  ∀ z (g1 g2 : z ~> x), f ∘ g1 = f ∘ g2 → g1 = g2.

Definition Epi {𝐂: precat} [x y: 𝐂] (f : x ~> y) :=
  Mono (𝐂 := 𝐂^op) f.

Lemma Mono_comp {𝐂: cat} [A B C: 𝐂] (f: A ~> B) (g: B ~> C):
  Mono f -> Mono g -> Mono (g ∘ f).
Proof.
  intros mono_f mono_g.
  intros z g1 g2 eq.
  rewrite 2!compoA in eq.
  apply mono_g in eq.
  by apply mono_f in eq.
Qed.

Lemma Mono_decomp {𝐂: cat} [A B C: 𝐂] (m: A ~> B)
(n: B ~> C): Mono (n ∘ m) -> Mono m.
Proof.
  intros mono.
  intros D f g eq.
  apply mono.
  by rewrite compoA eq -compoA.
Qed.

Lemma Epi_comp {𝐂: cat} [A B C: 𝐂] (f: A ~> B) (g: B ~> C):
  Epi f -> Epi g -> Epi (g ∘ f).
Proof.
  intros epi_f epi_g.
  apply (Mono_comp (𝐂 := 𝐂^op) g f epi_g epi_f).
Qed.

Lemma Epi_decomp {𝐂: cat} [A B C: 𝐂] (m: A ~> B)
(n: B ~> C): Epi (n ∘ m) -> Epi n.
Proof.
  intro epi.
  apply (Mono_decomp (𝐂 := 𝐂^op) n m epi).
Qed.

Definition joint_epi {𝐂: cat} [A B C: 𝐂] (f: A ~> C) (g: B ~> C) :=
  forall D (x y: C ~> D), x ∘ f = y ∘ f -> x ∘ g = y ∘ g -> x = y.

Definition joint_mono {𝐂: cat} [A B C: 𝐂] (f: C ~> A) (g: C ~> B) :=
  forall D (x y: D ~> C), f ∘ x = f ∘ y -> g ∘ x = g ∘ y -> x = y.