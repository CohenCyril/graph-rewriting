Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.

Local Open Scope cat.

Section Slice.

Context {𝐂 : cat} (C: 𝐂).

Definition Slice := {X: 𝐂 & X ~> C}.

HB.instance Definition _ := IsQuiver.Build Slice
  (fun x y => {f: (projT1 x) ~> (projT1 y) | projT2 y ∘ f = projT2 x }).

Definition Slice_id (x: Slice): x ~> x := exist _ idmap (comp1o (projT2 x)).

Definition Slice_comp (x y z: Slice) (f: x ~> y) (g: y ~> z): x ~> z.
  exists (proj1_sig g ∘ proj1_sig f).
  by rewrite -compoA (proj2_sig g) (proj2_sig f).
Defined.

HB.instance Definition _ := Quiver_IsPreCat.Build Slice
  Slice_id Slice_comp.

Lemma Slice_mapP (s1 s2 : Slice) (f g : s1 ~> s2) :
  proj1_sig f = proj1_sig g <-> f = g.
Proof.
  split.
  { intro.
  destruct f, g.
  simpl in *.
  revert e.
  rewrite H.
  intro.
  assert (e = e0).
  apply Prop_irrelevance.
  by rewrite H0. }
  intro.
  by rewrite H.
Qed.

Definition Slicecomp1o: forall (a b : Slice) (f : a ~> b), idmap \; f = f.
  intros.
  apply Slice_mapP.
  simpl.
  apply comp1o.
Defined.

Definition Slicecompo1: forall (a b : Slice) (f : a ~> b), f \; idmap = f.
  intros.
  apply Slice_mapP.
  simpl.
  apply compo1.
Defined.

Definition SlicecompoA: forall (a b c d : Slice) (f : a ~> b) (g : b ~> c) (h : c ~> d),
f \; (g \; h) = (f \; g) \; h.
  intros.
  apply Slice_mapP.
  simpl.
  apply compoA.
Defined.

HB.instance Definition _ := PreCat_IsCat.Build Slice Slicecomp1o Slicecompo1 SlicecompoA.
End Slice.

