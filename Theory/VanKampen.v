Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Morphism.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Terminal.
Require Import Unique.

Local Open Scope cat.

Definition VanKampen {𝐂: cat} [A B C D: 𝐂] (f: C ~> A) (g: C ~> B)
  (iA: A ~> D) (iB: B ~> D) (po: Pushout f g iA iB) :=
  ∀ A' B' C' D' (f': C' ~> A') (g': C' ~> B') (iA': A' ~> D') (iB': B' ~> D')
  (α: A' ~> A) (β: B' ~> B) (χ: C' ~> C) (δ: D' ~> D),
  iA' ∘ f' = iB' ∘ g' -> δ ∘ iA' = iA ∘ α -> δ ∘ iB' = iB ∘ β ->
  Pullback f α χ f' -> Pullback g β χ g' ->
  (Pullback iA δ α iA' ∧ Pullback iB δ β iB' ↔ Pushout f' g' iA' iB'). 

(* From Lack & Sobocinski 2005 *)
(*Definition VanKampen_alt {𝐂: cat} (_: HasPullbacks 𝐂) [A B C D: 𝐂]
        (a: A ~> B) (b: B ~> D) (c: A ~> C) (d: C ~> D) (po: Pushout a b c d) :=
    ∀ B' C' D' (β: B' ~> B) (χ: C' ~> C) (δ: D' ~> D)
        (b': B' ~> D') (d': C' ~> D'),
    δ ∘ b' = b ∘ β -> δ ∘ d' = d ∘ χ ->
    (Pullback b' δ β b ∧ Pullback d' δ χ d)
    ↔ ∃ A' (α: A' ~> A) (a': A' ~> B') (c': A' ~> C'),
    Pullback a' β α a ∧ Pullback c' χ α c ∧ Pushout a' b' c' d'.*)

(*Lemma VanKampen_equiv: ∀ {𝐂: cat} (hpb: HasPullbacks 𝐂) [A B C D: 𝐂]
    (a: A ~> B) (b: B ~> D) (c: A ~> C) (d: C ~> D) (po: Pushout a b c d),
    VanKampen a b c d po ↔ VanKampen_alt hpb a b c d po.
    Proof.
    intros.
    split.
    {intros.
    unfold VanKampen_alt.
    intros.
    destruct (Destruct_Pushout _ _ _ _ po).
    split.
    {intros [pb1 pb2].
    destruct hpb.
    destruct (Get_pb B' A B β a) as [A' [a' [α pb3]]].
    exists A'.
    exists α.
    exists a'.
    destruct (Get_pb C' A C χ c) as [A'2 [c' [α2 pb4]]].
    assert (pb31: Pullback (b' ∘ a') δ α (b ∘ a)).
    {eapply pb_comp.
    {apply pb1. }
    assumption. }
    assert (pb42: Pullback (d' ∘ c') δ α2 (d ∘ c)).
    {eapply pb_comp.
    {apply pb2. }
    assumption. }
    rewrite <-e in pb42.
    destruct (unique_pullback pb31 pb42) as [h [[h_comm1 h_comm2] h_unique]].
    exists (c' ∘ to h).
    split; [assumption|].
    assert (pb5: Pullback (c' ∘ h) χ α c).
    {rewrite Pb_iso_rewrite_A, <-h_comm2, <-comp_assoc, iso_to_from, id_right.
    assumption. }
    split; auto.
    rewrite <-(X A' B' C' D' a' b' (c' ∘ h) d' α β χ δ); auto.
    rewrite comp_assoc. symmetry. assumption. }
    intros [A' [α [a' [c' [pb1 [pb2 po']]]]]].
    rewrite (X A' B' C' D' a' b' c' d' α β χ δ); auto.
    apply (@pb_comm (𝐂^op)); auto. }
    intro.
    unfold VanKampen.
    intros.
    destruct (X B' C' D' β χ δ b' d'); auto.
    split.
    {intros [pb1 pb2].
    destruct s as [A'2 [α2 [a'2 [c'2 [pb3 [pb4 po'2]]]]]]; auto.

    destruct (Get_pb b' d') as [A'' [a'' [c'' pb5]]].
    destruct (Destruct_Pullback pb5).
    destruct (u A' a' c' X0) as [h1 [h1_comm1 h1_comm2] h1_unique].
    destruct (u A'2 a'2 c'2) as [h2 [h2_comm1 h2_comm2] h2_unique].
    {destruct (Destruct_Pushout _ _ _ _ po'2).
    assumption. }
    admit. }

    intro po'.
    destruct p; auto.
    {exists A'. exists α. exists a'. exists c'.
    auto. }
Admitted.*)

Lemma VK_mono: forall {𝐂: cat} [A B C D: 𝐂] (f: C ~> A) (g: C ~> B)
  (iA: A ~> D) (iB: B ~> D) (po: Pushout f g iA iB) (mono_f: Mono f),
   VanKampen f g iA iB po -> Mono iB ∧ Pullback iA iB f g.
Proof.
  intros ? ? ? ? ? ? ? ? ? ? ? VK.
  destruct (VK C B C B idmap g g idmap f idmap idmap iB) as [_ VK2];
    try rewrite compo1; try rewrite comp1o; auto.
  - symmetry. apply (po_comm po).
  - by apply Mono_trivial_pb.
  - apply Pb_along_id.
  - destruct VK2.
    apply Po_sym.
    apply Po_along_id.
  by split; [apply Mono_trivial_pb|].
Qed.