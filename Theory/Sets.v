Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
From HB Require Import structures.
Require Import cat_notations.
Require Import Pullback.
Require Import Pushout.
Require Import Reg_mono.
Require Import Adhesive.
Require Import Span.
Require Import ClassicalChoice.
Require Import FunctionalExtensionality.

Local Open Scope cat.

Definition Sets := Type.

HB.instance Definition _ := IsQuiver.Build
    Sets (fun X Y => X -> Y).

Lemma Precat_Sets: Quiver_IsPreCat Sets.
Proof.
  split.
  by intros ? ?.
  intros ? ? ? ? ? ?.
  auto.
Defined.

HB.instance Definition _ := Precat_Sets.

Lemma Cat_Sets: PreCat_IsCat Sets.
Proof.
  split.
  1,2: by intros ? ? ?.
  by intros ? ? ? ? ? ? ?.
Defined.

HB.instance Definition _ := Cat_Sets.

Definition Sets_pb_obj [X Y Z: Sets] (f: X ~> Z) (g: Y ~> Z) :=
  {a: X * Y | f (fst a) = g (snd a)}.

Definition Sets_pb_proj1 [X Y Z: Sets] (f: X ~> Z) (g: Y ~> Z):
  Sets_pb_obj f g ~> X :=
  fun a => fst (proj1_sig a).

Definition Sets_pb_proj2 [X Y Z: Sets] (f: X ~> Z) (g: Y ~> Z):
  Sets_pb_obj f g ~> Y :=
  fun a => snd (proj1_sig a).

Definition Sets_pb_span [X Y Z: Sets] (f: X ~> Z) (g: Y ~> Z):=
  Build_span (Sets_pb_proj1 f g) (Sets_pb_proj2 f g).

Lemma Pb_Sets: Cat_has_pb Sets.
Proof.
  constructor.
  intros X Y Z f g.
  exists (Sets_pb_obj f g).
  exists (Sets_pb_proj1 f g).
  exists (Sets_pb_proj2 f g).
  apply Build_Pullback.
  apply funext.
  by intros [[a1 a2] eq_a].
  intros P qA qB comm.
  simpl in P.
  unshelve econstructor.
  intro p.
  unshelve eapply (exist _ (qA p, qB p) _).
  simpl.
  apply (equal_f comm).
  simpl.
  split.
  apply funext.
  by intro p.
  apply funext.
  by intro p.

  intros v [eq_v1 eq_v2].
  apply funext.
  intro p.
  apply eq_sig_hprop.
  intros.
  apply Prop_irrelevance.
  simpl.
  rewrite eq_v1 eq_v2.
  unfold Sets_pb_proj1.
  unfold Sets_pb_proj2.
  by rewrite -surjective_pairing.
Defined.

HB.instance Definition _ := Pb_Sets.

Lemma Po_rm_Sets: Cat_has_po_along_reg_mono Sets.
Proof.
  constructor.
  intros X Y Z f g rm_f.
  