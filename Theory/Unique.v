Unset Universe Checking.
Require Import cat_notations.
Set Universe Checking.
Require Import cat.
Local Open Scope cat.

Record Unique {A: Type} (P : A → Type) := {
  unique_obj: A;
  unique_prop: P unique_obj;
  unique_uniqueness: ∀ v : A, P v → unique_obj = v;
}.

Definition Unique_morph {𝐂: quiver} [A B: 𝐂] (P : (A ~> B) → Type) :=
  @Unique (A ~> B) P.

(*Class Unique_morph {𝐂: quiver} [A B: 𝐂] (P : (A ~> B) → Type) := {
  unique_morph : A ~> B;
  unique_morph_property : P unique_morph;
  morph_uniqueness      : ∀ v : A ~> B, P v → unique_morph = v;
}.*)

Lemma Unicity {𝐂: quiver} [A B: 𝐂] [P: (A ~> B) -> Type]:
  Unique_morph P -> forall (u v: A ~> B), P u -> P v -> u = v.
Proof.
  intros.
  destruct X as [m Pm unique_m].
  assert (m = u).
  apply unique_m.
  assumption.
  subst.
  apply unique_m.
  assumption.
Qed.

Definition unique_morph {𝐂: quiver} [A B: 𝐂] [P: (A ~> B) -> Type]:
  Unique_morph P -> A ~> B := unique_obj P.

Definition unique_morph_prop {𝐂: quiver} [A B: 𝐂] [P: (A ~> B) -> Type] (u: Unique_morph P):
  P (unique_morph u) := unique_prop P u.

Definition unique_morph_uniqueness {𝐂: quiver} [A B: 𝐂] [P: (A ~> B) -> Type] (u: Unique_morph P):
  ∀ v : A ~> B, P v → (unique_morph u) = v := unique_uniqueness P u.

(*Arguments unique_obj {_ _} _.
Arguments unique_property {_ _} _.
Arguments uniqueness {_ _} _.*)

(*Arguments unique_morph {_ _ _ _} _.
Arguments unique_morph_property {_ _ _ _} _.
Arguments morph_uniqueness {_ _ _ _} _.*)

Coercion unique_morph: Unique_morph >-> hom.

Notation "∃! x .. y , P" := (Unique_morph (fun x => .. (Unique_morph (fun y => P)) ..))
  (at level 200, x binder, y binder, right associativity) : cat_scope.

Notation "' u" := (unique_morph_prop u) (at level 100): cat_scope.

Notation "'' u" := (unique_morph_uniqueness u) (at level 100): cat_scope.

(*Definition Unique_prop {𝐂: quiver} [A B: 𝐂] [P : (A ~> B) → Type]
  (u: Unique_morph P): P u := unique_morph_property u.*)
