From mathcomp Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Morphism.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Terminal.
Require Import Unique.
Require Import VanKampen.
Require Import Reg_mono.
Require Import Isomorphism.
Require Import Subobject.
Require Import Equalizer.
Require Import Adhesive.
Require Import Adhesive_morphisms.
Require Import Cokernel_pair.

Local Open Scope cat.

Section Adhesive_lemmas.

Context {𝐂: adhesive}.

Lemma Adhesive_mono_stable_po: forall [A B C D: 𝐂] (f: C ~> A) (g: C ~> B)
  (iA: A ~> D) (iB: B ~> D) (mono_f: Mono f),
  Pushout f g iA iB -> Mono iB.
Proof.
  intros ? ? ? ? ? ? ? ? ? po.
  pose (VK := Adhesive_VK _ _ _ _ _ _ _ _ mono_f po).
  by destruct (VK_mono _ _ _ _ po mono_f VK).
Qed.

End Adhesive_lemmas.

(* Adhesive and quasiadhesive categories - Lack & Sobocinski - 2005
Lemma 6.5 *)
Lemma Reg_mono_stable_PO {𝐂: rm_adhesive} [A B C D: 𝐂]
  [f: C ~> B] [g: A ~> D] [m: C ~> A] [n: B ~> D]
  (rm_m: Reg_mono m): Pushout m f g n -> Reg_mono n.
Proof.
  intro po_D.
  destruct (po_along_reg_mono m m rm_m) as [A' [p [q po_A']]].
  simpl in A'.
  destruct (Cokernel_rm_coproj po_A') as [rm_p rm_q].
  destruct (po_along_reg_mono q g rm_q) as [D' [h [s po_D']]].
  destruct (ump_po po_D (h ∘ p) (s ∘ n)) as [r [eq_r1 eq_r2] _].
  by rewrite compoA (po_comm po_A') -compoA (po_comm po_D') compoA
    (po_comm po_D) compoA.
  pose (po_is_pb po_D rm_m).
  destruct (po_mono_VK rm_q po_D' _ _ _ _ _ _ _
    _ _ _ _ _ (po_comm po_D) (eq_sym eq_r1) (eq_sym eq_r2)
    (Pb_sym (po_is_pb po_A' rm_m)) (po_is_pb po_D rm_m)) as [_ VK].
  destruct (VK po_D) as [_ pb_B].
  do 3 eexists.
  apply (Pb_equalizer pb_B).
Qed.

Lemma Reg_mono_comp {𝐂: rm_q_adhesive} [A B C: 𝐂]
  [m: B ~> C] [n: A ~> B]:
  Reg_mono m -> Reg_mono n -> Reg_mono (m ∘ n).
Proof.
  intros rm_m rm_n.
  apply Reg_mono_eq_adh in rm_m, rm_n.
  apply Reg_mono_eq_adh.
  apply (Adhesive_comp rm_m rm_n).
Qed.

Lemma exist_eq: forall [A] [P: A -> Prop] (x y: {x: A | P x}), proj1_sig x = proj1_sig y <-> x = y.
intros.
split.
intro.
destruct x, y.
simpl in *.
revert p0.
rewrite -H.
intros.
f_equal.
apply Prop_irrelevance.
intro.
by rewrite H.
Qed.

Lemma sub_inter_eq_pb {𝐂: rm_q_adhesive}: forall [A B C D: 𝐂] (a: A ~> B) (b: B ~> D)
  (c: A ~> C) (d: C ~> D) (mono_b: Mono b) (mono_d: Mono d),
  Pullback b d a c -> ∃ (mono_ab: Mono (b ∘ a)),
  Subobj_inter D (exist _ (B; b) mono_b) (exist _ (C; d) mono_d) (exist _ (A; b ∘ a) mono_ab).
Proof.
  simpl.
  intros ? ? ? ? ? ? ? ? ? ? pb.
  pose proof (mono_c := Mono_stable_PB pb mono_b).
  apply Pb_sym in pb.
  pose proof (mono_a := Mono_stable_PB pb mono_d).
  pose proof (mono_ab := Mono_comp _ _ mono_a mono_b).
  destruct (Destruct_Pullback pb) as [pb_comm ump_pb].
  exists mono_ab.
  unfold Subobj_inter.
  simpl.
  unshelve eexists.
  unshelve eapply (exist _ _ I).
  simpl.
  by unshelve eapply (exist _ a _).
  unshelve eexists.
  unshelve eapply (exist _ _ I).
  simpl.
  unshelve eapply (exist _ c pb_comm).
  simpl.
  intros.
  destruct I' as [[A' m] mono_m].
  simpl in *.
  destruct a' as [[a' eq_a'] t1].
  simpl in *.
  destruct b' as [[c' eq_c'] t2].
  simpl in *.
  unshelve epose (ump_pb A' c' a' _).
  by rewrite eq_a' eq_c'.
  unshelve eexists.
  unshelve eapply (exist _ _ I).
  simpl.
  unshelve eapply (exist _ (u :> A' ~> A) _).
  simpl.
  by rewrite compoA -('u).
  simpl.
  split.
  apply exist_eq.
  simpl.
  apply exist_eq.
  simpl.
  symmetry.
  apply ('u).
  apply exist_eq.
  simpl.
  apply exist_eq.
  simpl.
  symmetry.
  apply ('u).
  simpl.
  intros.
  destruct v as [[v eq_v] t3]; simpl in *.
  apply exist_eq.
  simpl.
  apply exist_eq.
  simpl.
  destruct H.
  apply exist_eq in e, e0.
  simpl in e, e0.
  apply exist_eq in e, e0.
  simpl in e, e0.
  apply (''u).
  by split; symmetry.
Qed.

(* Lack & Sobocinski - Adhesive and quasiadhesive categories - 2005
Theorem 5.1 *)
Lemma reg_subobj_union_is_subobj {𝐂: rm_q_adhesive}:
  forall [I A B C Z: 𝐂] [a: A ~> Z] [b: B ~> Z]
    (rm_a: Reg_mono a) (rm_b: Reg_mono b)
    [p: I ~> A] [u: A ~> C] [q: I ~> B] [v: B ~> C]
    (pb: Pullback a b p q) (po: Pushout p q u v)
    (c: C ~> Z) (eq_c1: c ∘ u = a) (eq_c2: c ∘ v = b), Mono c.
  Proof.
  intros.
  intros K f g eq_fg.

  pose proof (rm_q := Reg_mono_stable_PB _ _ _ _ pb rm_a).
  apply Pb_sym in pb.
  pose proof (rm_p := Reg_mono_stable_PB _ _ _ _ pb rm_b).

  destruct (Get_cube p q u v f (po_comm po))
    as [L [L1 [L2 [f' [f1 [f2 [l1' [l2' [l1 [l2
      [pb_f1 [pb_f2 [pb_f3 [pb_f4 comm_f]]]]]]]]]]]]]].
  pose proof (po_f := stable_po po rm_p pb_f1 pb_f3
    pb_f2 pb_f4 comm_f).
  pose proof (joint_epi_l1_l2 := Pushout_joint_epi po_f).

  destruct (Get_cube p q u v g (po_comm po))
    as [M [M1 [M2 [g' [g1 [g2 [m1' [m2' [m1 [m2
      [pb_g1 [pb_g2 [pb_g3 [pb_g4 comm_g]]]]]]]]]]]]]].
  pose proof (po_g := stable_po po rm_p pb_g1 pb_g3
    pb_g2 pb_g4 comm_g).
  pose proof (joint_epi_m1_m2 := Pushout_joint_epi po_g).

  destruct (Get_cube l1' l2' l1 l2 m1 (po_comm po_f))
    as [N [N11 [N12 [l' [l11 [l12 [m11' [m12' [m11 [m12 
      [pb_m11 [pb_m12 [pb_m13 [pb_m14 comm_m]]]]]]]]]]]]]].
  apply Pb_sym in pb_f1.
  pose proof (rm_l1' := Reg_mono_stable_PB _ _ _ _ pb_f1 rm_p). 
  pose proof (po_m := stable_po po_f rm_l1' pb_m11 pb_m13 pb_m12 pb_m14 comm_m).
  pose proof (joint_epi_m11_m12 := Pushout_joint_epi po_m).

  destruct (Get_cube l1' l2' l1 l2 m2 (po_comm po_f))
    as [N' [N21 [N22 [l'' [l21 [l22 [m21' [m22' [m21 [m22 
      [pb_m21 [pb_m22 [pb_m23 [pb_m24 comm_m']]]]]]]]]]]]]].
  pose proof (po_m' := stable_po po_f rm_l1' pb_m21 pb_m23 pb_m22 pb_m24 comm_m').
  pose proof (joint_epi_m21_m22 := Pushout_joint_epi po_m').

  apply joint_epi_m1_m2.

  { apply joint_epi_m11_m12.

  { assert (a ∘ (f1 ∘ l11) = a ∘ (g1 ∘ m11)).
    by rewrite -eq_c1 compoA -(compoA l11) -(pb_comm pb_f2) -2!compoA
      eq_fg compoA -(pb_comm pb_m12) compoA -(compoA m11) (pb_comm pb_g2)
      !compoA.
    apply (Reg_mono_is_mono _ rm_a) in H.
    by rewrite compoA (pb_comm pb_m12) -compoA (pb_comm pb_f2) compoA H
      -compoA -(pb_comm pb_g2).
  }

  unshelve epose (h := ump_pb pb (f2 ∘ l12) (g1 ∘ m12) _).
  by rewrite -eq_c2 -compoA (compoA f2) -(pb_comm pb_f4) -compoA eq_fg compoA
    -(pb_comm pb_m14) -compoA (compoA m1) (pb_comm pb_g2) -compoA eq_c1 compoA.
  by rewrite compoA (pb_comm pb_m14) -compoA (pb_comm pb_f4) compoA ('h) -compoA
    -(po_comm po) compoA -('h) -compoA -(pb_comm pb_g2).
  }

  apply joint_epi_m21_m22.

  { unshelve epose (h := ump_pb pb (g2 ∘ m21) (f1 ∘ l21) _).
  by rewrite -eq_c2 -compoA (compoA g2) -(pb_comm pb_g4) -compoA -eq_fg compoA
    (pb_comm pb_m22) -compoA (compoA l1) (pb_comm pb_f2) -compoA eq_c1 compoA.
  by rewrite compoA (pb_comm pb_m22) -compoA (pb_comm pb_f2) compoA ('h) -compoA
    (po_comm po) compoA -('h) -compoA -(pb_comm pb_g4).
  } 
  assert (b ∘ (f2 ∘ l22) = b ∘ (g2 ∘ m22)).
  by rewrite -eq_c2 compoA -(compoA l22) -(pb_comm pb_f4) -2!compoA
    eq_fg compoA -(pb_comm pb_m24) compoA -(compoA m22) (pb_comm pb_g4)
    !compoA.
  apply (Reg_mono_is_mono _ rm_b) in H.
  by rewrite compoA (pb_comm pb_m24) -compoA (pb_comm pb_f4) compoA H
    -compoA -(pb_comm pb_g4).
Qed.



(* On the axioms for adhesive and quasiadhesive categories - Garner & Lack - 2012
4.4 Proposition. If m : A → X is the union of regular subobjects
m1 : A1 → X and m2 : A2 → X in an rm-quasiadhesive category,
then m has a stable (epi,regular mono) factorization. *)
Lemma reg_union_epi_rm_fact_aux {𝐂: rm_q_adhesive}:
  forall [X A A0 A1 A2: 𝐂] (m1': A0 ~> A2) (n2: A2 ~> A) (m2': A0 ~> A1) (n1: A1 ~> A)
    (m1: A1 ~> X) (m2: A2 ~> X) (m: A ~> X) (rm_m1: Reg_mono m1) (rm_m2: Reg_mono m2)
    (eq_m1: m1 = m ∘ n1) (eq_m2: m2 = m ∘ n2) (pb_A0: Pullback m2 m1 m1' m2')
    (po_A: Pushout m1' m2' n2 n1),
    ∃ B Y X1 (n: B ~> X) (e: A ~> B) (i: X ~> X1) (j: X ~> X1)
      (q: X1 ~> Y), m = n ∘ e ∧ Reg_mono n ∧ Pushout m m (q ∘ i) (q ∘ j)
      ∧ Pushout n n (q ∘ i) (q ∘ j) ∧ Equalizer (q ∘ i) (q ∘ j) n.
Proof.
  intros.
  epose proof (mono_m := reg_subobj_union_is_subobj rm_m2 rm_m1
    pb_A0 po_A m (eq_sym eq_m2) (eq_sym eq_m1)).

  (* First let i, j : X ⇉ X1 be the cokernel pair of m1,
  and e1 : X1 → X the codiagonal. *)
  destruct (Get_cokernel_pair rm_m1) as [X1 [i [j po_X1]]].
  pose (e1 := ump_po po_X1 idmap idmap eq_refl).

  (* We can pull all these maps back along m2 to get the diagram
  https://q.uiver.app/#q=WzAsOCxbMCwwLCJBXzAiXSxbMCwxLCJBXzEiXSxbMSwxLCJYIl0sWzEsMCwiQV8yIl0sWzIsMSwiWF8xIl0sWzIsMCwiWF8yIl0sWzMsMSwiWCJdLFszLDAsIkFfMiJdLFswLDMsIm1fMSciXSxbMCwxLCJtXzInIiwyXSxbMSwyLCJtXzEiXSxbMywyLCJtXzIiLDJdLFszLDUsImlfMiIsMCx7Im9mZnNldCI6LTF9XSxbMyw1LCJqXzIiLDIseyJvZmZzZXQiOjF9XSxbMiw0LCJpIiwwLHsib2Zmc2V0IjotMX1dLFsyLDQsImoiLDIseyJvZmZzZXQiOjF9XSxbNSw0LCJsIiwyXSxbNSw3LCJlXzIiXSxbNyw2LCJtXzIiXSxbNCw2LCJlXzEiXV0=
  *)
  destruct (Get_pb m2 e1) as [X2 [e2 [l pb_X2]]].
  simpl in X2.

  unshelve epose proof (i2 := ump_pb pb_X2 idmap (i ∘ m2) _).
  by rewrite comp1o -compoA -('e1) compo1.
  assert (pb_A2_id1: Pullback m2 (e1 ∘ i) (e2 ∘ i2) m2).
  rewrite -('e1) -('i2).
  apply Pb_along_id.
  unshelve epose proof (pb_A21 := Pb_decomp _ pb_X2 pb_A2_id1).
  by rewrite -('i2).

  unshelve epose proof (j2 := ump_pb pb_X2 idmap (j ∘ m2) _).
  by rewrite comp1o -compoA -('e1) compo1.
  assert (pb_A2_id2: Pullback m2 (e1 ∘ j) (e2 ∘ j2) m2).
  rewrite -('e1) -('j2).
  apply Pb_along_id.
  unshelve epose proof (pb_A22 := Pb_decomp _ pb_X2 pb_A2_id2).
  by rewrite -('j2).

  (* in which l is a pullback of the regular monomorphism m2
  and so is itself a regular monomorphism. *)
  pose proof (rm_l := Reg_mono_stable_PB _ _ _ _ pb_X2 rm_m2).

  (* Since cokernel pairs of regular monomorphisms are
  pushouts along a regular monomorphism, they are stable under pullback,
  and so i2, j2 : A2 ⇉ X2 is the cokernel pair of m1',
  and e2 : X2 → A2 is the codiagonal. *)
  unshelve epose proof (po_X2 := stable_po po_X1 rm_m1 pb_A0 pb_A0 pb_A21 pb_A22 _).
  apply (Reg_mono_is_mono _ rm_l).
  by rewrite -2!compoA -('i2) -('j2) 2!compoA (pb_comm pb_A0)
  -2!compoA (po_comm po_X1).

  (* Since l is a regular monomorphism, we can form the pushout
  https://q.uiver.app/#q=WzAsNCxbMCwwLCJYXzIiXSxbMSwwLCJBXzIiXSxbMCwxLCJYXzEiXSxbMSwxLCJZIl0sWzAsMiwibCIsMl0sWzAsMSwiZV8yIl0sWzEsMywiayJdLFsyLDMsInEiLDJdXQ==
  *)
  destruct (po_along_reg_mono l e2 rm_l) as [Y [q [k po_Y]]].
  simpl in Y.

  (* We shall see that the maps qi, qj : X → Y
  are the cokernel pair of the union m : A → X. *)
  assert (cokern_m: Pushout m m (q ∘ i) (q ∘ j)).
  apply Build_Pushout.
  apply (Pushout_joint_epi po_A).
  by rewrite 4!compoA -eq_m2 -(pb_comm pb_A21) -(pb_comm pb_A22)
   -2! compoA (po_comm po_Y) 2!compoA -('i2) -('j2).
  by rewrite 4!compoA -eq_m1 (po_comm po_X1).
  (* To do this, suppose that u, v : X → Z are given with um = vm,
  or equivalently with um1 = vm1 and um2 = vm2. *)
  intros Z u v comm_uvm.
  (* Since um1 = vm1, there is a unique w : X1 → Z
  with wi = u and wj = v. *)
  unshelve epose (w := ump_po po_X1 u v _).
  by rewrite eq_m1 -2!compoA comm_uvm.

  (* On the other hand wli2 = wim2 = um2 = vm2 = wjm2 = wlj2
  and so the morphism wl out of the cokernel pair X2 of m1' agrees
  on the coprojections i2 and j2 of the cokernel pair,
  and therefore factorizes through the codiagonal e2,
  say as wl = w'e2. *)
  destruct (Codiagonal_fact po_X2 (fst ('i2)) (fst ('j2)) (w ∘ l)) as [w' eq_w'].
  by rewrite 2!compoA (pb_comm pb_A21) (pb_comm pb_A22) -2!compoA
  -2!('w) eq_m2 -2!compoA comm_uvm.

  (* By the universal property of the pushout Y,
  there is a unique w′'': Y → Z with w''q = w and w''k = w'. *)
  pose (w'' := ump_po po_Y w w' eq_w').

  (* Now w''qi = wi = u and w''qj = wj = v,
  and it is easy to see that w'' is unique with the property,
  thus proving that qi and qj give the cokernel pair of m. *)
  exists (w'' :> Y ~> Z).
  split; rewrite -compoA -('w''); apply ('w).
  intros w''' [eq_w'''1 eq_w'''2].
  apply (''w'').
  split.
  by apply (Pushout_joint_epi po_X1); rewrite compoA -('w).
  assert (w ∘ l ∘ i2 = w' ∘ e2 ∘ i2).
  by rewrite eq_w'.
  rewrite !compoA -(fst ('i2)) comp1o in H.
  by rewrite -H (pb_comm pb_A21) -compoA -('w) eq_w'''1 !compoA -(pb_comm pb_A21) -(compoA i2)
    (po_comm po_Y) compoA -('i2) comp1o.

  (* Since qi and qj are a cokernel pair,
  they have a common retraction (the codiagonal),
  and so we can form their equalizer
  n : B → X as their intersection;
  this is of course a regular monomorphism. *)
  destruct (Cokernel_equalizer cokern_m) as [B [n eq_n]]. (*[eq_n equalizer_n]]].*)

  exists B.
  exists Y.
  exists X1.
  exists n.

  (* Since qim = qjm, there is a unique map e : A → B with ne = m; *)
  pose proof (e := Eq_prop _ m (po_comm cokern_m)).

  exists e.
  exists i.
  exists j.
  exists q.
  split.
  apply ('e).
  split.
  eexists.
  exists (q ∘ i).
  exists (q ∘ j).
  assumption.
  split.
  assumption.
  split.
  rewrite ('e) in cokern_m.
  eapply (Cokern_fact cokern_m).
  by apply Eq_comp.
  assumption.
Qed.

Lemma reg_union_epi_rm_fact {𝐂: rm_q_adhesive}:
  forall [X A A0 A1 A2: 𝐂] (m1': A0 ~> A2) (n2: A2 ~> A) (m2': A0 ~> A1) (n1: A1 ~> A)
    (m1: A1 ~> X) (m2: A2 ~> X) (m: A ~> X) (rm_m1: Reg_mono m1) (rm_m2: Reg_mono m2)
    (eq_m1: m1 = m ∘ n1) (eq_m2: m2 = m ∘ n2) (pb_A0: Pullback m2 m1 m1' m2') (po_A: Pushout m1' m2' n2 n1),
    ∃ B (n: B ~> X) (e: A ~> B), m = n ∘ e ∧ Reg_mono n ∧ Epi e.
Proof.
  intros.
  destruct (reg_union_epi_rm_fact_aux m1' n2 m2' n1 m1 m2 m rm_m1 rm_m2 eq_m1 eq_m2 pb_A0 po_A)
    as [B [Y [X1 [n [e [i [j [q [eq_ne [rm_n [cokern_m [cokern_n equalizer_n]]]]]]]]]]]].
  exists B.
  exists n.
  exists e.
  split.
  assumption.
  split.
  assumption.

  (* we must show that e is a stable epimorphism.
  In fact it will suffice to show that it is an epimorphism,
  since all these constructions are stable under
  pullback along a map into X, thus if e is an epimorphism
  so will be any pullback. *)
  (* To see that e is an epimorphism, first note that m1 and m2
  factorize as mi = m(ni) = ne(ni), and now e(n1) and e(n2) are regular
  monomorphisms with union e. So once again we can factorize e
  as n'e', where n' is a regular monomorphism constructed as before. *)
  unshelve epose proof (s := reg_union_epi_rm_fact_aux _ _ _ _
    (e ∘ n1) (e ∘ n2) e _ _ eq_refl eq_refl _ po_A).

  { eapply (Reg_mono_decomp (e ∘ n1) n).
  by rewrite -compoA -eq_ne -eq_m1.
  apply (Reg_mono_is_mono _ rm_n). }

  { eapply (Reg_mono_decomp (e ∘ n2) n).
  by rewrite -compoA -eq_ne -eq_m2.
  apply (Reg_mono_is_mono _ rm_n). }

  { rewrite eq_m1 eq_m2 eq_ne in pb_A0.
  rewrite !compoA in pb_A0.
  apply (Pullback_mono (Reg_mono_is_mono _ rm_n) pb_A0). }

  {
    destruct s as [B' [Y' [X1' [n' [e' [i' [j' [q' [eq_e' [rm_n' [cokern_e [cokern_n' equalizer_n']]]]]]]]]]]].
    (* Observe that e is an epimorphism if and only if
    its cokernel pair is trivial, *)
    apply (Epi_cokern e).
    (* which in turn is equivalent to the equalizer n'
    of this cokernel pair being invertible. *)
    apply (Eq_cokern_invert equalizer_n' cokern_e).

    (* In an rm-adhesive category the regular monomorphisms are precisely
    the adhesive morphisms, and so are closed under composition.
    Thus the composite nn′ of the regular monomorphisms n and n′
    is itself a regular monomorphism. *)
    pose proof (rm_nn' := Reg_mono_comp rm_n rm_n').
    
    (* We conclude, using a standard argument,
    by showing that n' is invertible and so that e is an epimorphism. *)

    (* Observe that qi and qj are the cokernel pair of m = ne = nn'e',
    as well as of their equalizer n. But then they are also certainly
    the cokernel pair of nn'. *)
    assert (cokern_nn': Pushout (n ∘ n') (n ∘ n') (q ∘ i) (q ∘ j)).
    rewrite eq_ne eq_e' in cokern_m.
    apply Build_Pushout.
    by rewrite -compoA Eq_comp compoA.
    intros Y0 jA jB H.
    unshelve epose proof (ump_po cokern_m jA jB _).
    by rewrite -2!(compoA e') H !compoA. 
    assumption.

    (* Since this last is a regular monomorphism,
    it must be the equalizer of qi and qj, *)
    pose (equalizer_nn' := Pb_equalizer
      (po_is_pb cokern_nn' rm_nn')).
    (* but then it must be (isomorphic to) n, so that n' is
    invertible as claimed. *)
    pose (Eq_prop equalizer_nn' n (Eq_comp equalizer_n)).
    exists u.
    pose (Eq_prop equalizer_n n (Eq_comp equalizer_n)).
    apply (Unicity u0).
    rewrite -compoA.
    apply ('u).
    by rewrite comp1o.
    }
Qed.

(* Quasitoposes, Quasiadhesive Categories and Artin Glueing -
Johnstone et al. - 2007
Theorem 19. In [rm-adhesive] categories, binary unions of
regular subobjects are regular. *)
Lemma reg_subobj_union_is_reg_subobj {𝐂: rm_adhesive}:
  forall [U V W X Z: 𝐂] (sub_U: U ~> Z) (sub_V: V ~> Z)
    (rm_sub_U: Reg_mono sub_U) (rm_sub_V: Reg_mono sub_V)
    (π1: W ~> U) (π2: W ~> V) (u: U ~> X) (v: V ~> X)
    (pb_W: Pullback sub_U sub_V π1 π2) (po_X: Pushout π1 π2 u v)
    (sub_X: X ~> Z) (eq_u: sub_X ∘ u = sub_U) (eq_v: sub_X ∘ v = sub_V),
    Reg_mono sub_X.
Proof.
  (* Suppose that Z ∈ C and that U and V are two regular subobjects of Z [...] *)
  intros.

  (* Let X_ denote the smallest regular subobject of Z which contains X,
  ie the join of U and V in the lattice of regular subobjects of Z.
  This object can be obtained by factorising the map X → Z into an epi
  followed by a regular mono. *)
  destruct (reg_union_epi_rm_fact _ _ _ _ _ _ _ rm_sub_U rm_sub_V
    (eq_sym eq_u) (eq_sym eq_v)
    (Pb_sym pb_W) (Po_sym po_X))
    as [X_ [sub_X_ [x_ [eq_x_ [rm_sub_X_ epi_x_]]]]].

  (* We obtain objects A and A_ by constructing the pushouts
  in the left diagram below, where v_ = x_ v.
  Clearly v_ is regular mono by the usual cancellation properties. *)
  pose proof (rm_π2 := Reg_mono_stable_PB _ _ _ _ pb_W rm_sub_U).
  pose proof (rm_π1 := Reg_mono_stable_PB _ _ _ _ (Pb_sym pb_W) rm_sub_V).
  pose proof (rm_v := Reg_mono_stable_PO rm_π1 po_X).
  pose proof (rm_u := Reg_mono_stable_PO rm_π2 (Po_sym po_X)).
  pose (v_ := x_ ∘ v).
  assert (rm_v_: Reg_mono v_).
  rewrite -eq_v eq_x_ compoA -/v_ in rm_sub_V.
  apply (Reg_mono_decomp v_ sub_X_ rm_sub_V (Reg_mono_is_mono _ rm_sub_X_)).
  pose (u_ := x_ ∘ u).
  assert (rm_u_: Reg_mono u_).
  rewrite -eq_u eq_x_ compoA -/u_ in rm_sub_U.
  apply (Reg_mono_decomp u_ sub_X_ rm_sub_U (Reg_mono_is_mono _ rm_sub_X_)).

  destruct (po_along_reg_mono v v_ rm_v) as [A [p [q po_A]]].
  simpl in A.
  pose proof (rm_q := Reg_mono_stable_PO rm_v po_A).
  pose proof (rm_p := Reg_mono_stable_PO rm_v_ (Po_sym po_A)).

  destruct (po_along_reg_mono p x_ rm_p) as [A_ [a_ [j1 po_A_]]].
  simpl in A_.
  pose proof (rm_j1 := Reg_mono_stable_PO rm_p po_A_).
  pose (j2 := a_ ∘ q).

  assert (po'_A_: Pushout v_ v_ j1 j2).
  apply (Po_comp po_A (Po_sym po_A_)).
  pose proof (sub_A_ := ump_po po'_A_ sub_X_ sub_X_ eq_refl).

  (* The fact that the lower square is a pullback together with
    the fact that x_ is not an isomorphism implies that a_
    is not an isomorphism. *)
  assert (a__to_x__iso: (∃ a__, Iso_eq a_ a__)
    -> ∃ x__, Iso_eq x_ x__).
  intros [a__ [a__eq1 a__eq2]].
  pose (iso_a_ := Build_Iso a_ a__
    (Build_Iso_eq a__eq1 a__eq2)).
  assert (Pullback iso_a_¹ j1 p x_).
  unfold iso_a_.
  simpl.
  apply (po_is_pb po_A_ rm_p).
  apply (Iso_stable_pb _ X0).

  (* Now consider the second diagram above in which (‡)
  is a pushout and u_ = x_ u. *)
  destruct (po_along_reg_mono π1 π1 rm_π1) as [B [i2 [i1 po_B]]].
  simpl in B.
  pose proof (rm_i1 := Reg_mono_stable_PO rm_π1 po_B).
  pose proof (rm_i2 := Reg_mono_stable_PO rm_π1 (Po_sym po_B)).

  (* Using the fact that uπ1 = vπ2 the pushout of X and U along W is A
  and we obtain a map b : B → A such that the upper right square commutes
  and b i1 = p u.*)
  assert (eq_u__v_: u_ ∘ π1 = v_ ∘ π2).
  by rewrite compoA (po_comm po_X) -compoA.

  unshelve epose proof (b := ump_po po_B (q ∘ u_) (p ∘ u) _).
  by rewrite compoA eq_u__v_ -compoA -(po_comm po_A) compoA
    -(po_comm po_X) compoA.

  (* By the pasting properties of pushouts, this square is also a pushout. *)
  pose proof (po'_A := Po_comp (Po_sym po_X) (Po_sym po_A)).
  
  rewrite -eq_u__v_ ('b) in po'_A.
  unshelve epose proof (po''_A := Po_decomp _ po_B po'_A).
  apply ('b).

  assert (rm_b: Reg_mono b).
  apply (Reg_mono_stable_PO rm_u_ po''_A).

  (* Let h : B → U be the codiagonal (the unique map such that
  hi1 = hi2 =idU ) and let r be the unique map which satisfies
  r q = idX_ and r b = u_ h. *)
  pose proof (h := Cokernel_codiagonal po_B).

  unshelve epose proof (r := ump_po po''_A idmap (u_ ∘ h) _).
  by rewrite compo1 compoA -('h) comp1o.
  
  assert (po_X': Pushout b h r u_).
  apply Po_sym.
  eapply Po_decomp.
  by rewrite -('r).
  apply (Po_sym po''_A).
  rewrite -('h) -('r).
  apply Po_sym.
  apply Po_along_id.

  destruct (Get_pb sub_U sub_A_) as [B' [h' [b'_ pb_B']]].
  simpl in B'.

  assert (pb_U: Pullback sub_U sub_X_ idmap u_).
  apply Build_Pullback.
  by rewrite comp1o -compoA -eq_x_ eq_u.
  intros Y qA qB eq.
  exists qA.
  split.
  by rewrite compo1.
  rewrite -eq_u eq_x_ !compoA in eq.
  apply (Reg_mono_is_mono _ rm_sub_X_) in eq.
  by rewrite -eq compoA.
  intros v0 [eq_v0 _].
  by rewrite eq_v0 compo1.

  rewrite ('sub_A_) in pb_U.
  destruct (pb_fill pb_U pb_B') as [i'1 [eq_i'1 pb'_U]].
  rewrite -('sub_A_) (snd ('sub_A_)) in pb_U.
  destruct (pb_fill pb_U pb_B') as [i'2 [eq_i'2 pb''_U]].

  assert (pb'_W: Pullback u_ v_ π1 π2).
  apply Build_Pullback.
  by rewrite !compoA (po_comm po_X).
  intros Y qA qB eq.
  apply (ump_pb pb_W qA qB).
  by rewrite -eq_u -eq_v eq_x_ (compoA u) (compoA v) (compoA qA) (compoA qB)
    eq.

  pose proof (rm_b'_ := Reg_mono_stable_PB _ _ _ _ pb_B' rm_sub_U).
  
  unshelve epose proof (po_B' := stable_po po'_A_ rm_v_ pb'_W pb'_W pb'_U pb''_U _).
  apply (Reg_mono_is_mono _ rm_b'_).
  by rewrite -!compoA (pb_comm pb'_U) (pb_comm pb''_U) !(compoA _ u_)
    !eq_u__v_ -compoA (po_comm po'_A_) compoA.
  
  destruct (unique_pushout (Po_sym po_B') po_B)
    as [iso_B'B [eq_isoB'B2 eq_isoB'B1] _].
    
  pose (b_ := b'_ ∘ iso_B'B⁻¹).
  assert (eq_hh': (h: B ~> U) = h' ∘ iso_B'B⁻¹).
  apply (Pushout_joint_epi po_B).
  by rewrite -('h) -eq_isoB'B2 compoA -(compoA i'2)
    (iso_from_to (iso_eq iso_B'B)) compo1.
  by rewrite -('h) -eq_isoB'B1 compoA -(compoA i'1)
    (iso_from_to (iso_eq iso_B'B)) compo1.

  apply (Pullback_iso_D' (iso_sym iso_B'B)) in pb_B'.
  rename pb_B' into pb_B.
  cbn in pb_B.
  rewrite -eq_hh' -/b_ in pb_B.
  
  pose proof (s := Cokernel_codiagonal po'_A_).

  assert (eq_sub_A_: (sub_A_: A_ ~> Z) = sub_X_ ∘ s).
  apply (Pushout_joint_epi po'_A_);
  by rewrite -('sub_A_) compoA -('s) comp1o.
  rewrite eq_sub_A_ -(compo1 h) in pb_B.
  rewrite eq_sub_A_ compoA -('s) comp1o in pb_U.
  unshelve epose proof (pb'_B:= Pb_decomp _ pb_U pb_B).
  apply (Reg_mono_is_mono _ rm_sub_X_).
  by rewrite -(compoA b_) -(pb_comm pb_B) compo1 -eq_u
    eq_x_ !compoA.

  assert (po_X_: Pushout h b_ u_ s).
  apply (Build_Pushout (pb_comm pb'_B)).
  intros C g f eq_fg.

  pose proof (Epi_h := Cokernel_codiagonal_epi h po_B
  (fst ('h)) (snd ('h))).
  pose proof (Epi_s := Cokernel_codiagonal_epi s po'_A_
  (fst ('s)) (snd ('s))).

  exists (f ∘ j1).
  assert (f = (f ∘ j1) ∘ s).

  apply (Pushout_joint_epi po'_A_).
  by rewrite compoA -('s) comp1o.
  rewrite compoA -('s) comp1o.
  apply epi_x_.
  apply (Pushout_joint_epi po_X).

  rewrite compoA (compoA u) -/u_.

  by rewrite compoA -(pb_comm pb''_U) -(comp1o b'_)
  -(iso_from_to (iso_eq iso_B'B)) !(compoA i'2) eq_isoB'B2
  -(compoA i2) -/b_ -compoA -eq_fg compoA
  -('h) (snd ('h)) -compoA eq_fg (compoA u_)
  -(pb_comm pb'_U) compoA /b_ compoA
  -eq_isoB'B1 -(compoA i'1) (iso_from_to (iso_eq iso_B'B))
  compo1.
  rewrite compoA (compoA v) -/v_.
  by rewrite compoA -(po_comm po'_A_) !compoA.

  split.
  apply Epi_h.
  cbn.
  by rewrite eq_fg compoA (pb_comm pb'_B) -(compoA b_) -H.
  assumption.

  intros.
  by rewrite H compoA -('s) comp1o.

  pose proof (rm_b_ := Reg_mono_iso_r rm_b'_ (iso_sym iso_B'B)).

  assert (eq_b_: a_ ∘ b = b_).
  apply (Pushout_joint_epi po_B).
  by rewrite compoA -('b) -compoA -/j2 /b_ (compoA i2)
    -eq_isoB'B2 -(compoA i'2) (iso_from_to (iso_eq iso_B'B))
    compo1 (pb_comm pb''_U).
  by rewrite compoA -('b) -compoA (po_comm po_A_)
    /b_ (compoA i1) -eq_isoB'B1 -(compoA i'1) (iso_from_to (iso_eq iso_B'B))
    compo1 (pb_comm pb'_U) compoA.

  assert (eq_r: (r: A ~> X_) = s ∘ a_).
  apply (''r).
  split.
  by rewrite compoA -/j2 -('s).
  by rewrite (compoA b) eq_b_ (pb_comm pb'_B).
  
  assert (pb''_B: Pullback a_ b_ b idmap).
  assert (pb''_X_: Pullback (s ∘ a_) u_ b h).
  rewrite -eq_r.
  apply (po_is_pb po_X' rm_b).

  apply Pb_sym.
  apply Pb_sym in pb''_X_.
  rewrite -(comp1o h) in pb''_X_.
  unshelve eapply (Pb_decomp _ pb'_B pb''_X_).
  by rewrite comp1o.

  assert (idmap ∘ r = s ∘ a_).
  rewrite compo1.
  apply (''r).
  split.
  by rewrite compoA -/j2 -('s).
  by rewrite (compoA b) eq_b_ (pb_comm pb'_B).

  unshelve epose proof (po_mono_VK rm_b_
    (Po_sym po_X_) _ _ _ _ _ _ _ _ _ _ _ idmap
    (po_comm po_X') H _ (Pb_sym pb''_B) (Pb_along_id h)).
  by rewrite compo1 comp1o.
  destruct X0.
  destruct (p1 po_X').
  pose proof (Pb_along_id s).
  unshelve epose proof (a__ := ump_pb p2 idmap s _).
  by rewrite compo1 comp1o.

  unshelve epose proof (x__ := a__to_x__iso _).
  exists a__.
  constructor.
  by rewrite -('a__).

  unshelve epose proof (idA := ump_pb p2 a_ r _).
  apply (pb_comm p2).
  unshelve epose proof ((''idA) idmap _).
  split; by rewrite comp1o.
  destruct a__ as [a__ [a___eq1 a___eq2] unique_a__].
  simpl.
  apply (Unicity idA).
  split.
  destruct r as [r [eq_r1 eq_r2] unique_r]; simpl in *.
  by rewrite -compoA -a___eq1 compo1.
  by rewrite -(compoA a_) -a___eq2 (pb_comm p2) compo1.
  split; by rewrite comp1o.

  destruct x__ as [x__ ?].
  pose (iso := Build_Iso x_ x__ i).
  rewrite eq_x_.
  apply (Reg_mono_iso_r rm_sub_X_ iso).
Qed.
