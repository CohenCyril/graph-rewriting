Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
From HB Require Import structures.
Require Import cat_notations.
Require Import Pullback.
Require Import Adhesive.

Record Simple_graph := {
    Simple_V: Type;
    Simple_E: Simple_V -> Simple_V -> Prop
}.

Record Simple_graph_morph (g1 g2: Simple_graph) := {
    S_graph_morph_V:> Simple_V g1 -> Simple_V g2;
    S_graph_morph_E: forall v1 v2, Simple_E g1 v1 v2 ->
        Simple_E g2 (S_graph_morph_V v1) (S_graph_morph_V v2);
}.

Lemma Simple_graph_morphP (g1 g2: Simple_graph) (f h: Simple_graph_morph g1 g2):
  f = h ↔ S_graph_morph_V _ _ f = S_graph_morph_V _ _ h.
Proof.
  split.
  intro.
  by rewrite H.
  intro.
  destruct f, h.
  simpl in H.
  revert S_graph_morph_E0.
  rewrite H.
  intros.
  f_equal.
  apply Prop_irrelevance.
Qed.

HB.instance Definition _ := IsQuiver.Build
    Simple_graph Simple_graph_morph.

Lemma PreCat_S_graph: Quiver_IsPreCat Simple_graph.
Proof.
  constructor.
  intro a.
  by exists id.
  intros g1 g2 g3 [m1_V m1_E] [m2_V m2_E].
  unshelve econstructor.
  intro v.
  apply m2_V.
  by apply m1_V.
  intros v1 v2 e.
  apply m2_E.
  by apply m1_E.
Defined.

HB.instance Definition _ := PreCat_S_graph.

Lemma Cat_S_graph: PreCat_IsCat Simple_graph.
Proof.
  constructor.
  by intros g1 g2 [f_V f_E].
  by intros g1 g2 [f_V f_E].
  by intros g1 g2 g3 g4 [f_V f_E] [g_V g_E] [h_V h_E].
Defined.

HB.instance Definition _ := Cat_S_graph.

Lemma Pb_S_graph: Cat_has_pb Simple_graph.
Proof.
  constructor.
  intros g1 g2 g3 f g.
  (*intros g1 g2 g3 [f_V f_E] [g_V g_E].
  simpl.*)
  unshelve eexists.
  exists ({pbV: Simple_V g1 ∧ Simple_V g2 | f (fst pbV) = g (snd pbV)}).
  intros [[v1 v1'] _] [[v2 v2'] _].
  apply (Simple_E g1 v1 v2 /\ Simple_E g2 v1' v2').
  unshelve eexists.
  unshelve eexists.
  by intros [[? _] _].
  by intros [[? _] _] [[? _] _] [? _].
  simpl.
  unshelve eexists.
  unshelve eexists.
  by intros [[_ ?] _].
  by intros [[_ ?] _] [[_ ?] _] [_ ?].
  simpl.
  apply Build_Pullback.
  cbn.
  apply Simple_graph_morphP.
  simpl.
  apply funext.
  by intros [[? ?] ?].
  intros g4 [m_V m_E] [n_V n_E] eq.
  simpl in *.
  cbn in eq.
  apply Simple_graph_morphP in eq.
  simpl in eq.
  unshelve econstructor.
  unshelve eexists.
  simpl.
  intros v.
  
Qed.


Lemma Rm_Q_Adhesive_S_graph: Cat_is_Rm_Q_Adhesive Simple_graph.
