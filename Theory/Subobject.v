Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.
Require Import Subcategory.
Require Import Slice.
Require Import Morphism.
Require Import Isomorphism.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Pushout.
Require Import Unique.

Local Open Scope cat.

Section Subobject.

Context {𝐂: cat}.

Definition Subobj (C: 𝐂) := @Full_subcat (Slice C) (fun x => Mono (`2 x)).

(*Require Import CEquivalence.

Program Instance subobj_equiv (C: 𝐂): Equivalence _ := Build_Equivalence (fun (x y: Subobj C) => x ≅ y) _ _ _.

Next Obligation.
  intro.  apply (mono (f := g) (Mono := mono_g) _ _ _) in H.
  by apply mono in H.
Defined.

Next Obligation.
  repeat intro.
  pose (@iso_compose _ x y z).
  by apply i.
Defined.*)

End Subobject.

Definition Subobj_inter {𝐂: cat} (X: 𝐂) (A B I: Subobj X) :=
  ∃ (a: I ~> A) (b: I ~> B), forall I' (a': I' ~> A) (b': I' ~> B),
  ∃! (u: I' ~> I), a ∘ u = a' ∧ b ∘ u = b'.

Definition Subobj_union {𝐂: cat} (X: 𝐂) (A B U: Subobj X) :=
  ∃ (a: A ~> U) (b: B ~> U), forall U' (a': A ~> U') (b': B ~> U'),
  ∃! (u: U ~> U'), u ∘ a = a' ∧ u ∘ b = b'.
 
(*Definition Subobj_inter {𝐂: cat} [I A B X: 𝐂] (a: I ~> A) (b: A ~> X)
(c: I ~> B) (d: B ~> X) (mono_b: Mono b) (mono_d: Mono d) :=
  Pullback a b c d.

Definition Subobj_union {𝐂: cat} [I A B U: 𝐂] (a: I ~> A) (b: A ~> U)
(c: I ~> B) (d: B ~> U)
(inter: ∃ X (b': A ~> X) (d': B ~> X) (mono_b': Mono b') (mono_d': Mono d'),
  Subobj_inter a b' c d' mono_b' mono_d') :=
  Pushout a b c d.*)

(*Definition Subobj_inter {𝐂: cat_pb} [A B X: 𝐂] (m1: A ~> X) (m2: B ~> X)
  (mono_m1: Mono m1) (mono_m2: Mono m2): ∃ C (Um12: C ~> X), Mono Um12.
  destruct (Get_pb _ _ _ m1 m2) as [C [f [g pb]]].
  simpl in C.
  exists C.
  exists (m2 ∘ g).
  pose (Mono_stable_PB mono_m1).
  by apply (Mono_comp g m2).
Defined.*)